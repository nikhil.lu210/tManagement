<?php

namespace App\Http\Controllers;

use App\AllBus;
use App\AllRoute;
use App\AllDriver;
use Session;
use Illuminate\Http\Request;

class AllBusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('allbus.index')->with('allbuses', AllBus::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $allRoutes = AllRoute::all();
        // return view('allbus.create')->withAllRoutes($allRoutes);
        return view('allbus.create')
            ->with('allRoutes', AllRoute::all())
            ->with('allDrivers', AllDriver::all());
        $data->driverRoute = $this->get_driver_name_by_route_no($request->driverRoute);
    }

    //Route no matching
    public function get_driver_name_by_route_no($routeNo)
    {
        $route = DB::table('all_drivers')
                ->select('driverRoute')
                ->join('all_routes', 'all_routes.routeNo', '=', 'all_drivers.driverRoute')
                ->where('all_routes.routeNo', $routeNo)
                ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'busNo'=>'required',
            'routeNo'=>'required',
            'driverName'=>'required'
        ]);
        $busStore = AllBus::create([
            'busNo' => $request->busNo,
            'routeNo' => $request->routeNo,
            'driverName' => $request->driverName
        ]);
        // Session::flash('success', 'New Bus Added.');
        return redirect()->route('allbuses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $findBus = AllBus::find($id);
        return view('allbus.show')
            // ->with('allbus', $findBus)
            ->with('allbus', AllBus::find($id))
            ->with('allRoutes', AllRoute::all())
            ->with('allDrivers', AllDriver::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'busNo'=>'required',
            'routeNo'=>'required',
            'driverName'=>'required'
        ]);

        $allbus = AllBus::find($id);
        $allbus->busNo = $request->busNo;
        $allbus->routeNo = $request->routeNo;
        $allbus->driverName = $request->driverName;
        $allbus->save();
        // Session::flash('success', 'Bus Information Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allbus=AllBus::find($id);
        $allbus->delete();
        // Session::flash('success', 'Bus Deleted.');
        return redirect()->route('allbuses');
    }
}
