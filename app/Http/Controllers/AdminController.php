<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AllRoute;
use App\Routine;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = AllRoute::all();

        $day = date("D",strtotime(date('d-m-Y')));
        // dd($day);
        // $day = "Sun";
        $todayClass = null;
        $todayBus = null;
        $tomorrowClass = null;
        $tomorrowBus = null;
        switch($day){
            case "Sun":
                $todayClass = $this->getRoutine('Sunday');
                $tomorrowClass = $this->getRoutine('Monday');
                $todayBus = $this->getBus("Sunday"); 
                $tomorrowBus = $this->getBus("Monday"); 
                break;
            case "Mon":
                $todayClass = $this->getRoutine('Monday'); 
                $tomorrowClass = $this->getRoutine('Tuesday');
                $todayBus = $this->getBus("Monday"); 
                $tomorrowBus = $this->getBus("Tuesday");  
                break;
            case "Tue":
                $todayClass = $this->getRoutine('Tuesday'); ;
                $tomorrowClass = $this->getRoutine('Wednesday');
                $todayBus = $this->getBus("Tuesday"); 
                $tomorrowBus = $this->getBus("Wednesday"); 
                break;
            case "Wed":
                $todayClass = $this->getRoutine('Wednesday');
                $tomorrowClass = $this->getRoutine('Thrusday');
                $todayBus = $this->getBus("Wednesday"); 
                $tomorrowBus = $this->getBus("Thrusday"); 
                break;
            case "Thu":
                $todayClass = $this->getRoutine('Thrusday');
                $tomorrowClass = $this->getRoutine('Sunday');
                $todayBus = $this->getBus("Thrusday"); 
                $tomorrowBus = $this->getBus("Sunday"); 
                break;
            default:
                $todayClass = $this->getRoutine('Sunday'); 
                $tomorrowClass = $this->getRoutine('Monday');
                $todayBus = $this->getBus("Sunday"); 
                $tomorrowBus = $this->getBus("Monday"); 
        }
        // dd($todayClass);   

        return view('admin.home')->withRoutes($routes)
                    ->withTodayClass($todayClass)
                    ->withTomorrowClass($tomorrowClass)
                    ->withTodayBus($todayBus)
                    ->withTomorrowBus($tomorrowBus);
    }

    public function getRoutine($day){
        return Routine::select('id', 'day', 'busTime', 'batch', 'section')
                ->where(['day'=>$day, 'firstClass' => 1, 'refuse' => '0'])
                ->get();

    }
    public function getBus($day){
        return Routine::select('busTime')
        ->groupBy('busTime')
        ->where('day', 'Sunday')
        ->get();    
    }

    public function getRefuse(){
        $data =  Routine::select('id','refuse')
                ->where('firstClass', 1)
                ->where('refuse','!=','0')
                ->get();
        return response()->json($data);
    }

    public function removeRefuse(Request $request ){
          
        $data = Routine::find($request->id);
        $data->refuse = '0';
        $data->save();

        return;
    }
}
