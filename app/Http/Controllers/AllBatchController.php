<?php

namespace App\Http\Controllers;

use App\AllBatch;
use App\User;
use App\AllDepartment;
use Illuminate\Http\Request;
use Session;


class AllBatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = AllBatch::select('batchNo', 'deptShortName')->distinct()
        ->paginate(100);

        $mydata = null;

        // dd($data);
        
        foreach ($data as $key=>$one) {
            $mydata[$key] = AllBatch::where('deptShortName', $one->deptShortName)
                                ->where('batchNo', $one->batchNo)->count();
        }

        return view('allbatch.index')
            ->with('allbatches', $data )->with('count',$mydata);
            // ->with('alldepartments', AllDepartment::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('allbatch.create')
            ->with('allDepartments', AllDepartment::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'deptShortName'=>'required',
            'batchNo'=>'required',
            'startID'=>'required',
            'endID'=>'required',
            'studentPassword'=>'required|min:6'
        ]);

        // dd($request);

        for($i = $request->startID; $i <= $request->endID; $i++){
            $data  = new AllBatch();
            $data->deptShortName = $request->deptShortName;
            $data->batchNo = $request->batchNo;
            if($request->batchSection){
                $data->batchSection = $request->batchSection;
            }
            $std_pass = $this->generateRandomString($request->studentPassword);
            $data->studentID = $i;
            $data->studentPassword = $std_pass;
            $data->save();

            //Sends ID Password to User Table
            $dataUser  = new User();
            $dataUser->studentID = $i;
            $dataUser->studentDept = $request->deptShortName;
            $dataUser->studentBatch = $request->batchNo;
            if($request->batchSection){
                $dataUser->studentSection = $request->batchSection;
            }
            $dataUser->password = bcrypt($std_pass);
            $dataUser->save();
        }
        Session::flash('success', 'New Batch Added');
        
        return redirect()->route('allbatches');
    }
    // random function genarate
    function generateRandomString($characters) {
        $length = 6;
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
        // return bcrypt($randomString);
    } // close random function

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $deptName)
    {
        $arr = [
            'deptShortName' => $deptName,
             'batchNo'=> $id
        ];
        $data = AllBatch::where($arr)->paginate(100);
        return view('allbatch.show')->with('allbatches', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'deptShortName'=>'required',
            'batchNo'=>'required',
            'studentID'=>'required',
            'studentPassword'=>'required'
        ]);

        $allbatch = AllBatch::find($id);
        $allbatch->deptShortName = $request->deptShortName;
        $allbatch->batchNo = $request->batchNo;
        $allbatch->studentID = $request->studentID;
        $allbatch->studentPassword = $request->studentPassword;
        $allbatch->save();
        return redirect()->route('allbatches');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allbatch=AllBatch::find($id);
        $allbatch->delete();

        $user = User::find($id);
        $user->delete();

        return redirect()->back();
    }
}
