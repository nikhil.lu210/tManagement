<?php

namespace App\Http\Controllers;

use App\Routine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;

class RoutineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $sunday = Routine::select('*')->where('day','Sunday')->get();
        // $monday = Routine::select('*')->where('day','Monday')->get();
        // $tuesday = Routine::select('*')->where('day','Tuesday')->get();
        // $wednesday = Routine::select('*')->where('day','Wednesday')->get();
        // $thrusday = Routine::select('*')->where('day','Thrusday')->get();

        // dd($wednesday);

        return view('routine.index');
                // ->withSunday($sunday)
                // ->withMonday($monday)
                // ->withTuesday($tuesday)
                // ->withWednesday($wednesday)
                // ->withThrusday($thrusday);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('routine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'day'=>'required',
        //     'routineFile' => 'required'
        // ]);

        // ====================== destroy old data for this day ====================
        $deleteday = Routine::where('day',$request->day)->get();
        foreach($deleteday as $daydel){
            $delete = Routine::find($daydel->id);
            $delete->delete();
        }
        // ====================== destroy old data for this day ====================
 
        
        $upload=$request->file('routineFile');
        $filePath=$upload->getRealPath();

       
        $file=fopen($filePath, 'r');

        $header= fgetcsv($file);

        
        $escapedHeader=[];
        
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }

        while($columns=fgetcsv($file))
        {
            if($columns[0]=="")
            {
                continue;
            }

            $i = 2;
            $count = 1;
            while ( $i < count($columns) ) {
                if($columns[$i] == ""){$i++; continue; }
                if($columns[$i]){

                   $routine= new Routine();
                   $routine->day = $request->day;
                   $routine->firstClass = ($count == 1) ? 1:0;
                   if( $i == 2 ) {

                        $routine->classStartTime="09:30 AM";
                        $routine->classEndTime="10:55 AM";
                        $routine->busTime="08:30 AM";
                    }
                   elseif( $i == 6 ) {
                        $routine->classStartTime="11:00 AM";
                        $routine->classEndTime="12:25 PM";
                        $routine->busTime="09:45 AM";
                    }
                    elseif( $i == 10  ) {
                        $routine->classStartTime="12:30 PM";
                        $routine->classEndTime="01:55 PM";
                        $routine->busTime="11:45 AM";
                    }
                    elseif( $i == 14  ) {
                        $routine->classStartTime="02:00 PM";
                        $routine->classEndTime="03:25 PM";
                        $routine->busTime="12:45 PM";
                    }
                    elseif( $i == 18 ) {
                        $routine->classStartTime="03:30 PM";
                        $routine->classEndTime="05:00 PM";
                        $routine->busTime="12:45 PM";
                    }
                   $routine->batch=(int)$columns[0];
                   $routine->section=$columns[1];
                   $routine->course=$columns[$i];
                   $routine->teacher=$columns[$i+1];
                   $routine->room=$columns[$i+2];
                   // dd($routine);
                   $routine->save();
                   $i+=3;
                   $count++;

                }
            }           


        }

        return redirect()->route('routines');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($day, $time)
    {
        // $findRoutine = Routine::find($day, $time);
        // $datas = Routine::select('*')->where(['day' => $day, 'busTime' => $time])->get();
        $datas = Routine::where('day', $day)->where('busTime', $time)->get();
        
        return view('routine.show')->with('datas', $datas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'day'=>'required',
        //     'classStartTime'=>'required',
        //     'classEndTime'=>'required',
        //     'busTime'=>'required',
        //     'batch'=>'required',
        //     'section'=>'required',
        //     'course'=>'required',
        //     'teacher'=>'required',
        //     'room'=>'required'
        // ]);

        // $routine = Routine::find($id);
        // $routine->day = $request->day;
        // $routine->classStartTime = $request->classStartTime;
        // $routine->classEndTime = $request->classEndTime;
        // $routine->busTime = $request->busTime;
        // $routine->batch = $request->batch;
        // $routine->section = $request->section;
        // $routine->course = $request->course;
        // $routine->teacher = $request->teacher;
        // $routine->room = $request->room;
        // $routine->save();
        // return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $routine=Routine::find($id);
        $routine->delete();
        return redirect()->route('routines');
    }
}
