<?php

namespace App\Http\Controllers;

use App\MyProfile;
use Illuminate\Http\Request;
use App\User;
use App\AllDepartment;
use App\AllRoute;
use App\AllBatch;
use Illuminate\Support\Facades\Storage;
use Auth;
use Validator;
use Hash;
class MyProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('myprofile.index')->with('myprofile', User::find(Auth::user()->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('myprofile.create')->with('myprofile', User::find(Auth::user()->id))->with('allDepartments', AllDepartment::all())->with('allRoutes', AllRoute::all())->with('allbatch', AllBatch::find(Auth::user()->id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name'=>'required|string',
        //     'routeNo'=>'required',
        //     'pickPoint'=>'required',
        //     'studentMobile'=>'required',
        //     'parentMobile'=>'required',
        //     'email'=>'required|unique:users',
        //     'studentAddress'=>'required',
        // ]);
        // dd($request);
        // if($request->hasFile('avatar')){



        //     $avatar = $request->file('avatar');
        //     $mime = $avatar->getMimeType();

        //     if ($mime == "image/jpeg" || $mime == "image/png" || $mime == "image/svg+xml") {
        //         $this->validate($request,array(
        //             'avatar'    =>  'image|mimes:jpeg,png,jpg,svg|max:1024',
        //         ));
                                
        //     }
           
        // }


        $user = User::find(Auth::user()->id);

        if($request->hasFile('avatar')){
        
        $path = time().'.'.$request->avatar->getClientOriginalExtension();
        $request->avatar->move(public_path('images'), $path);
  
        $user->avatar = $path;
         }

        $user->name = $request->name;
        $user->routeNo = $request->routeNo;
        $user->pickPoint = $request->pickPoint;
        $user->studentMobile = $request->studentMobile;
        $user->parentMobile = $request->parentMobile;
        $user->email = $request->email;
        $user->studentAddress = $request->studentAddress;

        $user->save();

        return redirect()->route('myprofiles');
    }


    public function admin_credential_rules(array $data)
    {
        $messages = [
            'oldPassword.required' => 'Please enter current password',
            'newPassword.required' => 'Please enter password',
        ];

        $validator = Validator::make($data, [
            'oldPassword' => 'required',
            'newPassword' => 'required',
            'retypeNewPassword' => 'required|same:newPassword',     
        ], $messages);

        return $validator;
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function changePassword(Request $request){

        if(Auth::Check()){
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails()){  
                // return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
                Session::flash('error', 'password did not match, Please try again!!!');
                return redirect()->back();
            }
            else{  
                $current_password = Auth::User()->password;           
                if(Hash::check($request_data['oldPassword'], $current_password)){           
                    $user_id = Auth::User()->id;                       
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['newPassword']);
                    $obj_user->save(); 
                    Session::flash('success', 'password successfully changed!!!');
                    return redirect()->route('myprofiles');
                }
                else{           
                    $error = array('oldPassword' => 'Please enter correct current password');
                    return response()->json(array('error' => $error), 400);   
                }
            }        
        }
        else{
            return redirect()->back();
        }  
        
     }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
