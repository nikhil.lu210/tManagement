<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\AllDepartment;
use App\AllRoute;
use Illuminate\Http\Request;

use Mail;
use Session;

class AllTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('allteacher.index')->with('allteachers', Teacher::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('allteacher.create')
            ->with('allRoutes', AllRoute::all())
            ->with('allDepartments', AllDepartment::all());
    }

    // public function findRouteById($id){
    //     $data = AllRoute::select('stoppagePoint')
    //         ->where('routeNo' , $id)->get();

    //     return $data;
    // }
    public function findRouteById(Request $request){
        $data=AllRoute::select('stoppagePoint')
            ->where('routeNo', $request->id)
            //->take(100)
            ->get();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'teacherName'=>'required',
            'teacherShortName'=>'required',
            'teacherDepartment'=>'required',
            'teacherEmail'=>'required',
            'teacherPassword' =>'required|min:6',
        ]);

        // dd($request);
        $teacher = new Teacher();
        
        $teacher->name = $request->teacherName;
        $teacher->teacherShortName = $request->teacherShortName;
        $teacher->teacherDepartment = $request->teacherDepartment;
        $teacher->email = $request->teacherEmail;
        $teacher->password = bcrypt($request->teacherPassword);
        $teacher->save();

        $data = [
    		'name'	 =>	$request->teacherName,
			'email'	 => $request->teacherEmail,
			'subject' => "Transportation Management System",
			'password' => $request->teacherPassword,
    	];

        Mail::send('emails.contact', $data, function($message) use ($data) {
			$message->to($data['email']);
			$message->subject($data['subject']);
			$message->from('monopuras5@gmail.com');
	   });
        return redirect()->back();
        // return redirect()->route('allteachers'); //it returns to index page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('allteacher.show')
            ->with('allteacher', Teacher::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'teacherName'=>'required',
            'teacherShortName'=>'required',
            'teacherDepartment'=>'required',
            'teacherNumber'=>'required',
            'routeNo'=>'required',
            'pickPoint'=>'required'
        ]);

        $allteacher = AllTeacher::find($id);
        $allteacher->teacherName = $request->teacherName;
        $allteacher->teacherShortName = $request->teacherShortName;
        $allteacher->teacherDepartment = $request->teacherDepartment;
        $allteacher->teacherNumber = $request->teacherNumber;
        $allteacher->routeNo = $request->routeNo;
        $allteacher->pickPoint = $request->pickPoint;
        $allteacher->save();
        return redirect()->back(); //it returns into same page
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allteacher=AllTeacher::find($id);
        $allteacher->delete();
        return redirect()->route('allteachers');
    }


    // ===========< Custom made Functions >==========
    // public function prodfunct(){
	// 	$prod=ProductCat::all();
    //     return view('productlist',compact('prod'));
    // }
    
    // public function findProductName(Request $request){
    //     $data=Product::select('productname','id')
    //         ->where('prod_cat_id',$request->id)
    //         ->take(100)
    //         ->get();

    //     return response()->json($data);
    // }


}
