<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Teacher;
use App\Routine;

class TeacherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:teacher');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routine = Routine::where('teacher', Auth::guard('teacher')->user()->teacherShortName)
                    ->get();
        // dd($routine);
        $totalDay = null;
        foreach($routine as $key => $data){
            if( $key == 0 ) $totalDay[$key] = $data->day;
            else{
                $help = true;
                foreach($totalDay as $oneDay){
                    if($oneDay == $data->day) { $help = false; break; }
                }
                if($help){
                    $totalDay[$key] = $data->day;
                }
            }
        }
        return view('teacher.home')->withRoutines($routine)->withTotalday($totalDay);
        
    }

    public function refuseClass($id, $day){

        $curDay = date('D');
        $tmpDay = substr($day, 0, 3);

        $crntDayStatus = null;
        $rfsDayStatus = null;
        $days = ['Fri', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu'];
        foreach($days as $key => $day){
            if($day == $curDay) $crntDayStatus = $key;
            if($day == $tmpDay) $rfsDayStatus = $key;
        }
        $add = ($crntDayStatus < $rfsDayStatus) ? $rfsDayStatus - $crntDayStatus:0;
        if($crntDayStatus == 6){
             $add = $rfsDayStatus+1;
        }

        $routine = Routine::find($id);

        $date = strtotime("+".$add." day");

        $date = (string)date('Y-m-d',$date);

        $routine->refuse = $date;

        $routine->save();

        return redirect()->back();
    }
}
