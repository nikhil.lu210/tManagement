<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Routine;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routine = null;

        if(isset(Auth::user()->studentSection)){
            $routine = Routine::where('batch', Auth::user()->studentBatch)
                    ->where('section', Auth::user()->studentSection)
                    ->get();
        } else{
            $routine = Routine::where('batch', Auth::user()->studentBatch)
                    ->get();
        }

        // dd($routine);
        $totalDay = null;
        foreach($routine as $key => $data){
            if( $key == 0 ) $totalDay[$key] = $data->day;
            else{
                $help = true;
                foreach($totalDay as $oneDay){
                    if($oneDay == $data->day) { $help = false; break; }
                }
                if($help){
                    $totalDay[$key] = $data->day;
                }
            }
        }
        return view('home')->withRoutines($routine)->withTotalday($totalDay);
    }
}
