<?php

namespace App\Http\Controllers;

use App\AllDepartment;
use Illuminate\Http\Request;

class AllDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('alldepartment.index')->with('alldepartments', AllDepartment::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alldepartment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'departmentName'=>'required',
            'deptShortName'=>'required'
        ]);
        $busStore = AllDepartment::create([
            'departmentName' => $request->departmentName,
            'deptShortName' => $request->deptShortName
        ]);
        // Session::flash('success', 'New Bus Added.');
        return redirect()->route('alldepartments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('alldepartment.show')->with('alldepartment', AllDepartment::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'departmentName'=>'required',
            'deptShortName'=>'required'
        ]);

        $alldepartment = AllDepartment::find($id);
        $alldepartment->departmentName = $request->departmentName;
        $alldepartment->deptShortName = $request->deptShortName;
        $alldepartment->save();
        return redirect()->route('alldepartments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alldepartment=AllDepartment::find($id);
        $alldepartment->delete();
        return redirect()->route('alldepartments');
    }
}
