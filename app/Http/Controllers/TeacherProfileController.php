<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\AllRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use Validator;
use Hash;
use Session;

class TeacherProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::guard('teacher')->user()->id);
        $teacher = Teacher::find(Auth::guard('teacher')->user()->id);
        return view('teacherprofile.index')->with('teacher', $teacher);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = Teacher::find(Auth::guard('teacher')->user()->id);
        return view('teacherprofile.create')->with('teacher', $teacher)->with('allRoutes', AllRoute::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher = Teacher::find(Auth::guard('teacher')->user()->id);

        if($request->hasFile('avatar')){
        
        $path = time().'.'.$request->avatar->getClientOriginalExtension();
        $request->avatar->move(public_path('images'), $path);
  
        $teacher->avatar = $path;
         }

        $teacher->name = $request->name;
        $teacher->routeNo = $request->routeNo;
        $teacher->pickPoint = $request->pickPoint;
        $teacher->teacherNumber = $request->teacherMobile;
        $teacher->address = $request->address;

        $teacher->save();

        return redirect()->route('teacherprofiles');
    }



    public function admin_credential_rules(array $data)
    {
        $messages = [
            'oldPassword.required' => 'Please enter current password',
            'newPassword.required' => 'Please enter password',
        ];

        $validator = Validator::make($data, [
            'oldPassword' => 'required',
            'newPassword' => 'required',
            'retypeNewPassword' => 'required|same:newPassword',     
        ], $messages);

        return $validator;
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function changePassword(Request $request){

        
        if(Auth::guard('teacher')->user()){
 
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails()){  
                // return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
                Session::flash('error', 'password did not match, Please try again!!!');
                return redirect()->back();
            }
            else{ 
                $current_password = Auth::guard('teacher')->user()->password;
                            
                if(Hash::check($request_data['oldPassword'], $current_password)){       
                    $user_id = Auth::guard('teacher')->user()->id;                       
                    $obj_user = Teacher::find($user_id);
                    $obj_user->password = Hash::make($request_data['newPassword']);
                    $obj_user->save();
                    Session::flash('success', 'password successfully changed!!!');
                    return redirect()->route('teacherprofiles');
                }
                else{           
                    $error = array('oldPassword' => 'Please enter correct current password');
                    return response()->json(array('error' => $error), 400);   
                }
            }        
        }
        else{
            return redirect()->back();
        }  
        
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
