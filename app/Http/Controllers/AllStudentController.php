<?php

namespace App\Http\Controllers;

use App\AllStudent;
use App\AllDepartment;
use App\AllRoute;
use Illuminate\Http\Request;
use App\User;

class AllStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allstudent = User::paginate(50);
        // return print("Hello World");
        return view('allstudent.index')
            ->with('allstudents', $allstudent );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('allstudent.create')
            ->with('allRoutes', AllRoute::all())
            ->with('allDepartments', AllDepartment::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'studentID'=>'required',
            'studentName'=>'required',
            'studentDepartment'=>'required',
            'studentBatch'=>'required',
            'studentSection'=>'required',
            'routeNo'=>'required',
            'pickPoint'=>'required'
        ]);
        $studentStore = AllStudent::create([
            'studentID' => $request->studentID,
            'studentName' => $request->studentName,
            'studentDepartment' => $request->studentDepartment,
            'studentBatch' => $request->studentBatch,
            'studentSection' => $request->studentSection,
            'routeNo' => $request->routeNo,
            'pickPoint' => $request->pickPoint
        ]);
        // return redirect()->back();
        return redirect()->route('allstudents'); //it returns to index page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('allstudent.show')
            ->with('allstudent', User::find($id))
            ->with('allRoutes', AllRoute::all())
            ->with('allDepartments', AllDepartment::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'studentID'=>'required',
            'studentName'=>'required',
            'studentDepartment'=>'required',
            'studentBatch'=>'required',
            'studentSection'=>'required',
            'routeNo'=>'required',
            'pickPoint'=>'required'
        ]);

        $allstudent = AllStudent::find($id);
        $allstudent->studentID = $request->studentID;
        $allstudent->studentName = $request->studentName;
        $allstudent->studentDepartment = $request->studentDepartment;
        $allstudent->studentBatch = $request->studentBatch;
        $allstudent->studentSection = $request->studentSection;
        $allstudent->routeNo = $request->routeNo;
        $allstudent->pickPoint = $request->pickPoint;
        $allstudent->save();
        return redirect()->back(); //it returns into same page
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allstudent=User::find($id);
        $allstudent->delete();
        return redirect()->route('allstudents');
    }
}
