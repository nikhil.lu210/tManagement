<?php

namespace App\Http\Controllers;

use App\AllRoute;
use Illuminate\Http\Request;
use Session;
use App\User;

class AllRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Allroute::all();
        $totalStPoint = null;
        $totalStudent = null;
        foreach ($routes as $key => $route) {
            $i = 0;
            foreach(explode(',', $route->stoppagePoint) as $stoppagePoint){
                 $i++;
            }
            $totalStPoint[$key] = $i;
            $totalStudent[$key] = User::where('routeNo', $route->routeNo)->count();
        } 
        return view('allroute.index')
            ->with('allroutes', $routes)
            ->with('totalStPoint', $totalStPoint)
            ->with('totalStudent', $totalStudent);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('allroute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'routeNo'=>'required',
            'stoppagePoint'=>'required'
        ]);
        // $routeStore = AllRoute::create([
        //     'routeNo' => $request->routeNo,
        //     'stoppagePoint' => $request->stoppagePoint
        // ]);

      
        // foreach($request->stoppagePoint as $stoppage){
        //     $data  = new AllRoute();
        //     $data->routeNo = $request->routeNo;
        //     $data->stoppagePoint = $stoppage;

        //     $data->save();
        // }


        $data  = new AllRoute();
        $data->routeNo = $request->routeNo;
        $takeArray = '';
        foreach($request->stoppagePoint as $stoppage){
            $takeArray.=' , '.$stoppage;
        }
        $takeArray = substr($takeArray, 3, strlen($takeArray));
        $data->stoppagePoint = $takeArray;
        $data->save();


        // Session::flash('success', 'New Route Added.');
        return redirect()->route('allroutes');
        // dd($routeStore);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $findRoute = AllRoute::find($id);
        return view('allroute.show')->with('allroute', $findRoute);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'routeNo'=>'required',
            'stoppagePoint'=>'required'
        ]);

        $allroute = AllRoute::find($id);
        $allroute->routeNo = $request->routeNo;
        
        $takeArray = '';
        foreach($request->stoppagePoint as $stoppage){
            $takeArray.=','.$stoppage;
        }
        $takeArray = substr($takeArray, 3, strlen($takeArray));
        $allroute->stoppagePoint = $takeArray;
        $allroute->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allroute=AllRoute::find($id);
        $allroute->delete();
        return redirect()->route('allroutes');
    }
}
