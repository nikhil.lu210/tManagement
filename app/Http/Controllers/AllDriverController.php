<?php

namespace App\Http\Controllers;

use App\AllDriver;
use App\AllRoute;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class AllDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('alldriver.index')->with('alldrivers', AllDriver::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alldriver.create')
            ->with('allRoutes', AllRoute::all())
            ->with('allDrivers', AllDriver::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'driverID'=>'required',
            'driverName'=>'required',
            'driverMobile'=>'required',
            'driverAddress'=>'required',
            'driverLicence'=>'required',
            'driverNID'=>'required',
            // 'driverPhoto'=>'required',
            'driverRoute'=>'required'
        ]);

        $driver = new AllDriver();

        $driver->driverID = $request->driverID;
        $driver->driverName = $request->driverName;
        $driver->driverMobile = $request->driverMobile;
        $driver->driverAddress = $request->driverAddress;
        $driver->driverLicence = $request->driverLicence;
        $driver->driverNID = $request->driverNID;
        $driver->driverRoute = $request->driverRoute;

        if($request->hasFile('driverPhoto')){
            $path = time().'.'.$request->driverPhoto->getClientOriginalExtension();
            $request->driverPhoto->move(public_path('images'), $path);
            $driver->driverPhoto = $path;
         }


         

         $driver->save();

        // return redirect()->back();
        return redirect()->route('alldrivers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('alldriver.show')
            ->with('alldriver', AllDriver::find($id))
            ->with('allRoutes', AllRoute::all())
            ->with('allDrivers', AllDriver::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'driverID'=>'required',
            'driverName'=>'required',
            'driverMobile'=>'required',
            'driverAddress'=>'required',
            'driverLicence'=>'required',
            'driverNID'=>'required',
            // 'driverPhoto'=>'required',
            'driverRoute'=>'required'
        ]);

        $alldriver = AllDriver::find($id);
        $alldriver->driverID = $request->driverID;
        $alldriver->driverName = $request->driverName;
        $alldriver->driverMobile = $request->driverMobile;
        $alldriver->driverAddress = $request->driverAddress;
        $alldriver->driverLicence = $request->driverLicence;
        $alldriver->driverNID = $request->driverNID;
        // $alldriver->driverPhoto = $request->driverPhoto;
        $alldriver->driverRoute = $request->driverRoute;
        $alldriver->save();
        return redirect()->route('alldrivers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alldriver=AllDriver::find($id);
        $alldriver->delete();
        return redirect()->route('alldrivers');
    }
}
