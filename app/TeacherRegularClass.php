<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherRegularClass extends Model
{
    protected $fillable = [
        'classDay',
        'classTime',
        'busTime',
        'routeNo',
        'pickPoin',
        'dropPoint',
        'classTeacher' 
    ];
}
