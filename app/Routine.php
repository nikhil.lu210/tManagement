<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{
    protected $fillable = [
        'day',
        'firstClass',
        'classStartTime',
        'classEndTime',
        'busTime',
        'batch',
        'section',
        'course',
        'teacher',
        'room'
    ];
}
