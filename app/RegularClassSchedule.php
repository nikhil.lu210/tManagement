<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegularClassSchedule extends Model
{
    protected $fillable = [
        'classDay',
        'classTime',
        'busTime',
        'routeNo',
        'pickPoin',
        'dropPoint',
        'classTeacher' 
    ];
}
