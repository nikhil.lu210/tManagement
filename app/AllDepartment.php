<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDepartment extends Model
{
    protected $fillable = [
        'departmentName',
        'deptShortName'
    ];
}
