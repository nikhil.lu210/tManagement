<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllStudent extends Model
{
    protected $fillable = [
        'studentID',
        'studentName',
        'studentNumber',
        'studentDepartment',
        'studentBatch',
        'studentSection',
        'routeNo',
        'pickPoint'
    ];
}
