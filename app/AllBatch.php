<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllBatch extends Model
{
    protected $fillable = [
        'deptShortName',
        'batchNo',
        'batchSection',
        'studentID',
        'studentPassword'
    ];
}
