<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherRoute extends Model
{
    protected $fillable = [
        'teacherName',
        'teacherShortName',
        'teacherID',
        'routeNo',
        'pickPoint'
    ];
}
