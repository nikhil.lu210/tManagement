<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllBus extends Model
{
    protected $fillable = [
        'busNo',
        'routeNo',
        'driverName'
    ];
}
