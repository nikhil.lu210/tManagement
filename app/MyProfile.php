<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyProfile extends Model
{
    protected $fillable = [
        'studentID',
        'studentName',
        'studentNumber',
        'studentDepartment',
        'studentBatch',
        'studentSection',
        'routeNo'
    ];
}
