<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDriver extends Model
{
    protected $fillable = [
        'driverID',
        'driverName',
        'driverMobile',
        'driverAddress',
        'driverLicence',
        'driverNID',
        'driverPhoto',
        'driverRoute'
    ];
}
