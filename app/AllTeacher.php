<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllTeacher extends Model
{
    protected $fillable = [
        'teacherName',
        'teacherShortName',
        'teacherDepartment',
        'teacherEmail',
        'teacherPassword'
    ];
}
