<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyRoute extends Model
{
    protected $fillable = [
        'studentName',
        'studentID',
        'routeNo',
        'pickPoin',
        'dropPoint'
    ];
}
