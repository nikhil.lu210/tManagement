<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherProfile extends Model
{
    protected $fillable = [
        'studentName',
        'studentDepartment',
        'studentBatch',
        'studentSection',
        'studentID',
        'routeNo'
    ];
}
