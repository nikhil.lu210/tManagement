<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllRoute extends Model
{
    protected $fillable = [
        'routeNo',
        'stoppagePoint'
    ];
}
