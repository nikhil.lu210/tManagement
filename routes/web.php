<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Admin Authentication Route From here
Route::get('admin/home', 'AdminController@index');

Route::get('admin/home/getRefuse', 'AdminController@getRefuse');
Route::post('admin/home/refuse', 'AdminController@removeRefuse');

Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin', 'Admin\LoginController@login');

Route::get('admin/register', 'Admin\RegisterController@showRegistrationForm')->name('admin.register');
Route::post('admin/register', 'Admin\RegisterController@register');

Route::get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::post('admin-password/reset', 'Admin\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');


//Teacher Authentication Route From here
Route::get('teacher/home', 'TeacherController@index');

Route::get('teacher', 'Teacher\LoginController@showLoginForm')->name('teacher.login');
Route::post('teacher', 'Teacher\LoginController@login');

Route::get('teacher/register', 'Teacher\RegisterController@showRegistrationForm')->name('teacher.register');
Route::post('teacher/register', 'Teacher\RegisterController@register');

Route::get('teacher-password/reset', 'Teacher\ForgotPasswordController@showLinkRequestForm')->name('teacher.password.request');
Route::post('teacher-password/email', 'Teacher\ForgotPasswordController@sendResetLinkEmail')->name('teacher.password.email');
Route::post('teacher-password/reset', 'Teacher\ResetPasswordController@reset')->name('teacher.password.update');
Route::get('teacher-password/reset/{token}', 'Teacher\ResetPasswordController@showResetForm')->name('teacher.password.reset');


/*==================< Student Section >==============*/

//Regular Class Schedule
Route::get('/regularclassschedules', [
	'uses' => 'RegularClassScheduleController@index',
	'as' => 'regularclassschedules'
]);
Route::get('/regularclassschedule/create', [
	'uses' => 'RegularClassScheduleController@create',
	'as' => 'regularclassschedule.create'
]);
Route::post('/regularclassschedule/store', [
	'uses' => 'RegularClassScheduleController@store',
	'as' => 'regularclassschedule.store'
]);
Route::get('/regularclassschedule/show/{id}', [
	'uses' => 'RegularClassScheduleController@show',
	'as' => 'regularclassschedule.show'
]);
Route::post('/regularclassschedule/update/{id}', [
	'uses' => 'RegularClassScheduleController@update',
	'as' => 'regularclassschedule.update'
]);
Route::get('/regularclassschedule/destroy/{id}',[
	'uses'=>'RegularClassScheduleController@destroy',
	'as'=>'regularclassschedule.destroy'
]);

//Extra Class Schedule
Route::get('/extraclassschedules', [
	'uses' => 'ExtraClassScheduleController@index',
	'as' => 'extraclassschedules'
]);

Route::get('/extraclassschedule/create', [
	'uses' => 'ExtraClassScheduleController@create',
	'as' => 'extraclassschedule.create'
]);

Route::post('/extraclassschedule/store', [
	'uses' => 'ExtraClassScheduleController@store',
	'as' => 'extraclassschedule.store'
]);

Route::get('/extraclassschedule/show/{id}', [
	'uses' => 'ExtraClassScheduleController@show',
	'as' => 'extraclassschedule.show'
]);

Route::post('/extraclassschedule/update/{id}', [
	'uses' => 'ExtraClassScheduleController@update',
	'as' => 'extraclassschedule.update'
]);

Route::get('/extraclassschedule/destroy/{id}',[
	'uses'=>'ExtraClassScheduleController@destroy',
	'as'=>'extraclassschedule.destroy'
]);

//My Profile Schedule
Route::get('/myprofile', [
	'uses' => 'MyProfileController@index',
	'as' => 'myprofiles'
]);
Route::get('/myprofile/create', [
	'uses' => 'MyProfileController@create',
	'as' => 'myprofile.create'
]);
Route::post('/myprofile/store', [
	'uses' => 'MyProfileController@store',
	'as' => 'myprofile.store'
]);


Route::post('/myprofile/changepassword', [
	'uses' => 'MyProfileController@changePassword',
	'as' => 'myprofile.changePassword'
]);

Route::get('/myprofile/show/{id}', [
	'uses' => 'MyProfileController@show',
	'as' => 'myprofile.show'
]);
Route::post('/myprofile/update/{id}', [
	'uses' => 'MyProfileController@update',
	'as' => 'myprofile.update'
]);
Route::get('/myprofile/destroy/{id}',[
	'uses'=>'MyProfileController@destroy',
	'as'=>'myprofile.destroy'
]);

//My Route Schedule
Route::get('/myroutes', [
	'uses' => 'MyRouteController@index',
	'as' => 'myroutes'
]);
Route::get('/myroute/create', [
	'uses' => 'MyRouteController@create',
	'as' => 'myroute.create'
]);
Route::post('/myroute/store', [
	'uses' => 'MyRouteController@store',
	'as' => 'myroute.store'
]);
Route::get('/myroute/show/{id}', [
	'uses' => 'MyRouteController@show',
	'as' => 'myroute.show'
]);
Route::post('/myroute/update/{id}', [
	'uses' => 'MyRouteController@update',
	'as' => 'myroute.update'
]);
Route::get('/myroute/destroy/{id}',[
	'uses'=>'MyRouteController@destroy',
	'as'=>'myroute.destroy'
]);



/*==================< Teacher Section >==============*/

/* Teacher Regular Classes */
Route::get('/teacher/teacherregularclass', [
	'uses' => 'TeacherRegularClassController@index',
	'as' => 'teacherregularclasses'
]);
Route::get('/teacher/teacherregularclass/create', [
	'uses' => 'TeacherRegularClassController@create',
	'as' => 'teacherregularclass.create'
]);
Route::post('/teacher/teacherregularclass/store', [
	'uses' => 'TeacherRegularClassController@store',
	'as' => 'teacherregularclass.store'
]);
Route::get('/teacher/teacherregularclass/show/{id}', [
	'uses' => 'TeacherRegularClassController@show',
	'as' => 'teacherregularclass.show'
]);
Route::post('/teacher/teacherregularclass/update/{id}', [
	'uses' => 'TeacherRegularClassController@update',
	'as' => 'teacherregularclass.update'
]);
Route::get('/teacher/teacherregularclass/destroy/{id}',[
	'uses'=>'TeacherRegularClassController@destroy',
	'as'=>'teacherregularclass.destroy'
]);


/* Teacher Extra Classes */
Route::get('/teacher/teacherextraclass', [
	'uses' => 'TeacherExtraClassController@index',
	'as' => 'teacherextraclasses'
]);
Route::get('/teacher/teacherextraclass/create', [
	'uses' => 'TeacherExtraClassController@create',
	'as' => 'teacherextraclass.create'
]);
Route::post('/teacher/teacherextraclass/store', [
	'uses' => 'TeacherExtraClassController@store',
	'as' => 'teacherextraclass.store'
]);
Route::get('/teacher/teacherextraclass/show/{id}', [
	'uses' => 'TeacherExtraClassController@show',
	'as' => 'teacherextraclass.show'
]);
Route::post('/teacher/teacherextraclass/update/{id}', [
	'uses' => 'TeacherExtraClassController@update',
	'as' => 'teacherextraclass.update'
]);
Route::get('/teacher/teacherextraclass/destroy/{id}',[
	'uses'=>'TeacherExtraClassController@destroy',
	'as'=>'teacherextraclass.destroy'
]);


// Teacher Class Refuse
Route::get('/teacher/class/refuse/{id}/{day}',[
	'uses'=>'TeacherController@refuseClass',
	'as'=>'class.refuse'
]);

/* Teacher Profile */
Route::get('teacher/teacherprofiles', [
	'uses' => 'TeacherProfileController@index',
	'as' => 'teacherprofiles'
]);
Route::get('teacher/teacherprofile/create', [
	'uses' => 'TeacherProfileController@create',
	'as' => 'teacherprofile.create'
]);
Route::post('teacher/teacherprofile/store', [
	'uses' => 'TeacherProfileController@store',
	'as' => 'teacherprofile.store'
]);

Route::post('teacher/teacherprofile/changepass', [
	'uses' => 'TeacherProfileController@changePassword',
	'as' => 'teacherprofile.changepassword'
]);

Route::get('teacher/teacherprofile/show/{id}', [
	'uses' => 'TeacherProfileController@show',
	'as' => 'teacherprofile.show'
]);
Route::post('teacher/teacherprofile/update/{id}', [
	'uses' => 'TeacherProfileController@update',
	'as' => 'teacherprofile.update'
]);
Route::get('teacher/teacherprofile/destroy/{id}',[
	'uses'=>'TeacherProfileController@destroy',
	'as'=>'teacherprofile.destroy'
]);


/* Teacher Route */
Route::get('teacher/teacherroutes', [
	'uses' => 'TeacherRouteController@index',
	'as' => 'teacherroutes'
]);
Route::get('teacher/teacherroute/create', [
	'uses' => 'TeacherRouteController@create',
	'as' => 'teacherroute.create'
]);
Route::post('teacher/teacherroute/store', [
	'uses' => 'TeacherRouteController@store',
	'as' => 'teacherroute.store'
]);
Route::get('teacher/teacherroute/show/{id}', [
	'uses' => 'TeacherRouteController@show',
	'as' => 'teacherroute.show'
]);
Route::post('teacher/teacherroute/update/{id}', [
	'uses' => 'TeacherRouteController@update',
	'as' => 'teacherroute.update'
]);
Route::get('teacher/teacherroute/destroy/{id}',[
	'uses'=>'TeacherRouteController@destroy',
	'as'=>'teacherroute.destroy'
]);



/*==================< Teacher Section >==============*/


/* All Students Route */
Route::get('admin/allstudents', [
	'uses' => 'AllStudentController@index',
	'as' => 'allstudents'
]);
// Route::get('admin/allstudents', 'AllStudentController@index')->name('allstudents');
Route::get('admin/allstudent/create', [
	'uses' => 'AllStudentController@create',
	'as' => 'allstudent.create'
]);
Route::post('admin/allstudent/store', [
	'uses' => 'AllStudentController@store',
	'as' => 'allstudent.store'
]);
Route::get('admin/allstudent/show/{id}', [
	'uses' => 'AllStudentController@show',
	'as' => 'allstudent.show'
]);
Route::post('admin/allstudent/update/{id}', [
	'uses' => 'AllStudentController@update',
	'as' => 'allstudent.update'
]);
Route::get('admin/allstudent/destroy/{id}',[
	'uses'=>'AllStudentController@destroy',
	'as'=>'allstudent.destroy'
]);


/* All Teachers Route */
Route::get('admin/allteachers', [
	'uses' => 'AllTeacherController@index',
	'as' => 'allteachers'
]);
Route::get('admin/allteacher/create', [
	'uses' => 'AllTeacherController@create',
	'as' => 'allteacher.create'
]);

Route::get('admin/allteacher/getRoute' , 'AllTeacherController@findRouteById');

Route::post('admin/allteacher/store', [
	'uses' => 'AllTeacherController@store',
	'as' => 'allteacher.store'
]);
Route::get('admin/allteacher/show/{id}', [
	'uses' => 'AllTeacherController@show',
	'as' => 'allteacher.show'
]);
Route::post('admin/allteacher/update/{id}', [
	'uses' => 'AllTeacherController@update',
	'as' => 'allteacher.update'
]);
Route::get('admin/allteacher/destroy/{id}',[
	'uses'=>'AllTeacherController@destroy',
	'as'=>'allteacher.destroy'
]);


/* All Routes Route */
Route::get('admin/allroutes', [
	'uses' => 'AllRouteController@index',
	'as' => 'allroutes'
]);
Route::get('admin/allroute/create', [
	'uses' => 'AllRouteController@create',
	'as' => 'allroute.create'
]);
Route::post('admin/allroute/store', [
	'uses' => 'AllRouteController@store',
	'as' => 'allroute.store'
]);
Route::get('admin/allroute/show/{id}', [
	'uses' => 'AllRouteController@show',
	'as' => 'allroute.show'
]);
Route::post('admin/allroute/update/{id}', [
	'uses' => 'AllRouteController@update',
	'as' => 'allroute.update'
]);
Route::get('admin/allroute/destroy/{id}',[
	'uses'=>'AllRouteController@destroy',
	'as'=>'allroute.destroy'
]);


/* All Buses Route */
Route::get('admin/allbuses', [
	'uses' => 'AllBusController@index',
	'as' => 'allbuses'
]);
Route::get('admin/allbus/create', [
	'uses' => 'AllBusController@create',
	'as' => 'allbus.create'
]);
Route::post('admin/allbus/store', [
	'uses' => 'AllBusController@store',
	'as' => 'allbus.store'
]);
Route::get('admin/allbus/show/{id}', [
	'uses' => 'AllBusController@show',
	'as' => 'allbus.show'
]);
Route::post('admin/allbus/update/{id}', [
	'uses' => 'AllBusController@update',
	'as' => 'allbus.update'
]);
Route::get('admin/allbus/destroy/{id}',[
	'uses'=>'AllBusController@destroy',
	'as'=>'allbus.destroy'
]);



/* All Drivers Route */
Route::get('admin/alldrivers', [
	'uses' => 'AllDriverController@index',
	'as' => 'alldrivers'
]);
Route::get('admin/alldriver/create', [
	'uses' => 'AllDriverController@create',
	'as' => 'alldriver.create'
]);
Route::post('admin/alldriver/store', [
	'uses' => 'AllDriverController@store',
	'as' => 'alldriver.store'
]);
Route::get('admin/alldriver/show/{id}', [
	'uses' => 'AllDriverController@show',
	'as' => 'alldriver.show'
]);
Route::post('admin/alldriver/update/{id}', [
	'uses' => 'AllDriverController@update',
	'as' => 'alldriver.update'
]);
Route::get('admin/alldriver/destroy/{id}',[
	'uses'=>'AllDriverController@destroy',
	'as'=>'alldriver.destroy'
]);



/* ==============< Routine Upload Part >============ */
Route::get('admin/routines', [
	'uses' => 'RoutineController@index',
	'as' => 'routines'
]);
Route::get('admin/routine/create', [
	'uses' => 'RoutineController@create',
	'as' => 'routine.create'
]);
Route::post('admin/routine/store', [
	'uses' => 'RoutineController@store',
	'as' => 'routine.store'
]);
Route::get('admin/routine/show/{day}/at/{time}', [
	'uses' => 'RoutineController@show',
	'as' => 'routine.show'
]);
Route::post('admin/routine/update/{id}', [
	'uses' => 'RoutineController@update',
	'as' => 'routine.update'
]);
Route::get('admin/routine/destroy/{id}',[
	'uses'=>'RoutineController@destroy',
	'as'=>'routine.destroy'
]);



/* All Departments Route */
Route::get('admin/alldepartments', [
	'uses' => 'AllDepartmentController@index',
	'as' => 'alldepartments'
]);
Route::get('admin/alldepartment/create', [
	'uses' => 'AllDepartmentController@create',
	'as' => 'alldepartment.create'
]);
Route::post('admin/alldepartment/store', [
	'uses' => 'AllDepartmentController@store',
	'as' => 'alldepartment.store'
]);
Route::get('admin/alldepartment/show/{id}', [
	'uses' => 'AllDepartmentController@show',
	'as' => 'alldepartment.show'
]);
Route::post('admin/alldepartment/update/{id}', [
	'uses' => 'AllDepartmentController@update',
	'as' => 'alldepartment.update'
]);
Route::get('admin/alldepartment/destroy/{id}',[
	'uses'=>'AllDepartmentController@destroy',
	'as'=>'alldepartment.destroy'
]);


/* All Batches Route */
Route::get('admin/allbatches', [
	'uses' => 'AllBatchController@index',
	'as' => 'allbatches'
]);
Route::get('admin/allbatch/create', [
	'uses' => 'AllBatchController@create',
	'as' => 'allbatch.create'
]);
Route::post('admin/allbatch/store', [
	'uses' => 'AllBatchController@store',
	'as' => 'allbatch.store'
]);
Route::get('admin/allbatch/show/{id}/{deptName}', [
	'uses' => 'AllBatchController@show',
	'as' => 'allbatch.show'
]);
Route::post('admin/allbatch/update/{id}', [
	'uses' => 'AllBatchController@update',
	'as' => 'allbatch.update'
]);
Route::get('admin/allbatch/destroy/{id}',[
	'uses'=>'AllBatchController@destroy',
	'as'=>'allbatch.destroy'
]);