<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAllDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driverID')->unique()->max(11)->min(11);
            $table->string('driverName');
            $table->string('driverMobile')->unique();
            $table->string('driverAddress');
            $table->string('driverLicence')->unique();
            $table->string('driverNID')->unique();
            $table->binary('driverPhoto')->nullable();
            $table->string('driverRoute')->nullable();
            $table->timestamps();

            // DB::statement("ALTER TABLE all_drivers  AUTO_INCREMENT = 1234;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_drivers');
    }
}
