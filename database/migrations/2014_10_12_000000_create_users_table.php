<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('avatar')->nullable();
            $table->string('studentID')->unique();
            $table->string('name')->nullable();
            $table->string('studentDept')->nullable();
            $table->string('studentBatch')->nullable();
            $table->string('studentSection')->nullable();
            $table->integer('routeNo')->nullable();
            $table->string('pickPoint')->nullable();
            $table->string('studentMobile')->nullable();
            $table->string('parentMobile')->nullable();
            $table->string('email')->nullable();
            $table->text('studentAddress')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
