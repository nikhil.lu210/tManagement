<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_batches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deptShortName');
            $table->string('batchNo');
            $table->string('batchSection')->nullable();
            $table->string('studentID');
            $table->string('studentPassword');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_batches');
    }
}
