<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherExtraClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_extra_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('classDay');
            $table->string('classTime');
            $table->string('busTime');
            $table->integer('routeNo');
            $table->string('pickPoint');
            $table->string('dropPoint');
            $table->string('classTeacher');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_extra_classes');
    }
}
