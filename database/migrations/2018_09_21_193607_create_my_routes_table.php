<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('studentName');
            $table->integer('studentID');
            $table->integer('routeNo');
            $table->string('pickPoint');
            $table->string('dropPoint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_routes');
    }
}
