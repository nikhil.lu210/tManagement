<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('teacherName');
            $table->string('teacherShortName');
            $table->integer('teacherID');
            $table->string('routeNo');
            $table->string('pickPoint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_routes');
    }
}
