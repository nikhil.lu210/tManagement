<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegularClassSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regular_class_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('classDay');
            $table->string('classTime');
            $table->string('busTime');
            $table->integer('routeNo');
            $table->string('pickPoint');
            $table->string('dropPoint');
            $table->string('classTeacher');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regular_class_schedules');
    }
}
