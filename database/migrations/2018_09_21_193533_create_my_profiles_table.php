<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('studentName');
            $table->string('studentNumber')->unique()->max(11)->min(11);
            $table->string('studentDepartment');
            $table->string('studentBatch');
            $table->string('studentSection');
            $table->integer('studentID');
            $table->integer('routeNo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_profiles');
    }
}
