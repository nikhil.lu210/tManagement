<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('teacherName');
            $table->string('teacherShortName');
            $table->string('teacherEmail')->unique();
            $table->string('teacherDepartment');
            $table->string('teacherPassword');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_teachers');
    }
}
