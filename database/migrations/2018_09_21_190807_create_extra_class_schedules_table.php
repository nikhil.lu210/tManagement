<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraClassSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_class_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('day');
            $table->integer('addRemove');
            $table->string('classStartTime');
            $table->string('classEndTime');
            $table->string('busTime');
            $table->integer('batch');
            $table->string('section');
            $table->string('course');
            $table->string('teacher');
            $table->string('room');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_class_schedules');
    }
}
