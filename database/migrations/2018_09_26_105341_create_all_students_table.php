<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_students', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('studentID')->unique();
            $table->string('studentName');
            $table->string('studentNumber')->unique()->max(11)->min(11);
            $table->string('studentDepartment');
            $table->string('studentBatch');
            $table->string('studentSection')->nullable();
            $table->string('routeNo');
            $table->string('pickPoint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_students');
    }
}
