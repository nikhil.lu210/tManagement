<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    // $depth = $faker->randomElement('CSE');
    // $section = $faker->randomElement(['A', 'B', 'C', 'D', null]);
    $stId = '151'.(string)rand(1,9).(string)rand(1,9).(string)rand(1,9).(string)rand(1,9).(string)rand(1,9).(string)rand(1,9).(string)rand(1,9);

    $pickPoint = $faker->randomElement(['Tilagor Point' , 'Baluchar Point' , 'Eidgah Point' , 'Ambarkhana Point' , 'Subidbazar Point' , 'Madina Market' , 'Temukhi Point' , 'Kamalbazar Point']);
    $batch = rand(38,48);
    return [
        // 'avatar' => 'https://image.flaticon.com/icons/png/128/236/236831.png',
        // 'studentId' => $faker->unique()->randomNumber,
        'studentId' => $stId,
        'name' => $faker->name,
        'studentDept' => 'CSE',
        'studentBatch' => (string)$batch,
        'studentSection' => 'E',
        // 'studentSection' => $section,
        'routeNo' => (int)rand(1,4),
        'pickPoint' => $pickPoint,
        'studentMobile' => '01712345678',
        'parentMobile' => '01712345678',
        'email' => $faker->unique()->safeEmail,
        'studentAddress' => $faker->sentence,
        'password' => bcrypt('123456'), 
        'remember_token' => str_random(10),
    ];
});
