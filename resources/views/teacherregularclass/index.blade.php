@extends('layouts.app-teacher')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('teacher/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Teachers Portal</li>
                        <li class="breadcrumb-item active" aria-current="page">Regular Class Schedule</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
@endsection
