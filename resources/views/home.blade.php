@extends('layouts.app')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Student Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 student-dashboard">
                <div class="card">
                    <div class="card-header">
                        <h4>Daily Routine</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="owl-carousel owl-theme" id="dailyRoutine">
                                @isset($totalday)
                                @foreach($totalday as $today)
                                    <div class="item">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>{{ $today }}</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-dark">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Day</th>
                                                                <th scope="col">Bus Time</th>
                                                                <th scope="col">Class Time</th>
                                                                <th scope="col">Subject</th>
                                                                <th scope="col">Room No</th>
                                                                <th scope="col">Teacher</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($routines as $routine)
                                                         @if($routine->day == $today)
                                                            <tr>
                                                                <td class="fw_700">{{ $routine->day }}</td>
                                                                <td class="fw_700">{{ $routine->busTime }}</td>
                                                                <td>{{ $routine->classStartTime }} - {{ $routine->classEndTime }}</td>
                                                                <td>{{ $routine->course }}</td>
                                                                <td>{{ $routine->room }}</td>
                                                                <td>{{ $routine->teacher }}</td>
                                                            </tr>
                                                         @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="row m-t-30">
            <div class="col-md-12 student-dashboard">
                <div class="card">
                    <div class="card-header">
                        <h4>Extra Class Routine</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="owl-carousel owl-theme" id="extraRoutine">
                                    <div class="item">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Sunday</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-dark">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Bus Time</th>
                                                                <th scope="col">Class Time</th>
                                                                <th scope="col">Subject</th>
                                                                <th scope="col">Room No</th>
                                                                <th scope="col">Teacher</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Monday</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-dark">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Bus Time</th>
                                                                <th scope="col">Class Time</th>
                                                                <th scope="col">Subject</th>
                                                                <th scope="col">Room No</th>
                                                                <th scope="col">Teacher</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Tuesday</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-dark">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Bus Time</th>
                                                                <th scope="col">Class Time</th>
                                                                <th scope="col">Subject</th>
                                                                <th scope="col">Room No</th>
                                                                <th scope="col">Teacher</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fw_700">08.30 AM</td>
                                                                <td>9.30 AM - 11.00 AM</td>
                                                                <td>CSE-2311</td>
                                                                <td>309</td>
                                                                <td>MAK</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</section>
@endsection
