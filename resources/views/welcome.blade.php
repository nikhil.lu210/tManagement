<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>LU Transportation | HOME</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Alegreya+SC:400,500,700,800,900" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('files/css/style.css')}}">

        <!-- Styles -->
        <style>
            html, body {
                color: #fff;
                font-family: 'Muli', sans-serif;
                font-family: 'Alegreya SC', serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .homepage-part{
                background-image: url('../files/images/carousel_background.jpg') !important;
                background-size: cover;
              }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 6px;
                top: 30px;
            }

            .content {
                text-align: center;
            }
            .content h1{
                font-size: 50px;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #000;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                font-weight: bold;
                background-color: #009688;
                border-color: #009688;
                padding: 16px 40px;
                border-radius: 5px;
                margin: 3px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .welcome{
                background: #151e27eb;
                padding: 50px 70px;
                border-radius: 30px;
                border: 2px solid #090a0e;
            }

        </style>
    </head>
    <body>
        <section class="homepage-part flex-center position-ref full-height">
            
            {{--  @if (Route::has('teacher.login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('teacher/home') }}">Home</a>
                    @else
                        <a href="{{ route('teacher.login') }}">Teacher Login</a>
                        <a href="{{ route('teacher.register') }}">Teacher Registration</a>
                    @endauth
                </div>
            @endif  --}}

            <div class="content">
                <div class="welcome">
                    <img src="{{ asset('files/images/logo.png')}}" alt="LOGO" class="img-responsive" style="max-width: 40%; padding-bottom: 50px;">
                    <br>
                    {{-- <h1>Welcome To</h1>
                    <h1>LEADING UNIVERSITY</h1> --}}
                    @if (Route::has('login'))
                        <div class="links">
                            @auth
                                <a href="{{ url('/home') }}">Home</a>
                            @else
                                <a href="{{ route('login') }}">Student Login</a>
                                <a href="{{ route('teacher.login') }}">Teacher Login</a>
                                {{--  <a href="{{ route('register') }}">Student Register</a>  --}}
                            @endauth
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </body>
</html>
