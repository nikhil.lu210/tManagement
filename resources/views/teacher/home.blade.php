@extends('layouts.app-teacher')

@section('stylesheet')
<link rel="stylesheet" href="{{ asset('admin-css/css/owl/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ asset('admin-css/css/owl/owl.theme.default.min.css')}}">
@endsection

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('teacher/home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Teacher Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 p-b-20">
                        <div class="today-classes">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Daily Routine</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 m-b-20">
                                            <div class="owl-carousel owl-theme" id="dailyRoutine">
                                            @foreach ($totalday as $today)
                                                
                                            
                                                <div class="item">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h4 class="float-left">{{ $today }}</h4>
                                                            {{-- <button class="btn btn-danger float-right btn-sm m-r-5"> Refuse All</button> --}}
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-dark">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Bus Time</th>
                                                                            <th scope="col">Class Time</th>
                                                                            <th scope="col">Batch</th>
                                                                            <th scope="col">Subject</th>
                                                                            <th scope="col">Room No</th>
                                                                            <th scope="col">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($routines as $routine)
                                                                    @if($routine->day == $today)
                                                                        <tr>
                                                                            <td class="fw_700">{{ $routine->busTime }}</td>
                                                                            <td>{{ $routine->classStartTime }} - {{ $routine->classEndTime }}</td>
                                                                            <td>{{ $routine->batch }} <sup>{{ $routine->section }}</sup></td>
                                                                            <td>{{ $routine->course }}</td>
                                                                            <td>{{ $routine->room }}</td>
                                                                            <td>
                                                                                <?php
                                                                                    $curDay = date('D');
                                                                                    $tmpDay = substr($routine->day, 0, 3);
                                                                                    $crntDayStatus = null;
                                                                                    $rfsDayStatus = null;
                                                                                    $days = ['Fri', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu'];
                                                                                    foreach($days as $key => $day){
                                                                                        if($day == $curDay) $crntDayStatus = $key;
                                                                                        if($day == $tmpDay) $rfsDayStatus = $key;
                                                                                    }
                                                                                    $status = ( $crntDayStatus > $rfsDayStatus ) ? 0:1;
                                                                                    if($crntDayStatus == 6) $status = 1;
                                                                                ?>
                                                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                                                    <a href="{{ route('class.refuse', ['id' => $routine, 'day' => $routine->day]) }}" class="btn btn-danger custom-btn btn-sm @if($status == 0 || $routine->refuse != 0)disabled @endif">
                                                                                        <i class="fas fa-arrow-alt-circle-right"></i>
                                                                                        Refuse
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-none">
                    <div class="col-md-12 p-b-20">
                        <div class="extra-classes">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Extra Class Routine</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 m-b-20">
                                            <div class="owl-carousel owl-theme" id="extraRoutine">
                                                <div class="item">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h4 class="float-left">Sunday</h4>
                                                            <div class="btn-group float-right" role="group" aria-label="Basic example">
                                                                <a href="#" class="btn btn-danger custom-btn btn-sm">
                                                                    <i class="fas fa-arrow-alt-circle-right"></i>
                                                                    Refuse
                                                                </a>
                    
                                                                <a href="#" class="btn btn-info custom-btn btn-sm">
                                                                    <i class="fas fa-info-circle"></i> 
                                                                    Add Class
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-dark">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Bus Time</th>
                                                                            <th scope="col">Class Time</th>
                                                                            <th scope="col">Batch</th>
                                                                            <th scope="col">Subject</th>
                                                                            <th scope="col">Room No</th>
                                                                            <th scope="col">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="fw_700">08.30 AM</td>
                                                                            <td>9.30 AM - 11.00 AM</td>
                                                                            <td>38 <sup>(E)</sup></td>
                                                                            <td>CSE-2311</td>
                                                                            <td>309</td>
                                                                            <td>
                                                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                                                    <a href="#" class="btn btn-danger custom-btn btn-sm">
                                                                                        <i class="fas fa-arrow-alt-circle-right"></i>
                                                                                        Refuse
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('admin-css/js/owl/owl.carousel.min.js')}}"></script>
<script>
$('#dailyRoutine').owlCarousel({
    loop: true,
    margin: 10,
    autoHeight:true,
    nav: true,
    dots: false,
    navText: ['<i class="fa fa-2x fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-2x fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  })
</script>
@endsection
