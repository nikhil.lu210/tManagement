<!DOCTYPE html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <title>LU Transportation | ADMIN LOGIN</title>

    {{-- favicon icon --}}
    <link rel="icon" href="{{ asset('login-signup/images/logo.png')}}"> 
    
    {{-- Google Font --}}
    <link href="https://fonts.googleapis.com/css?family=Philosopher:400,700" rel="stylesheet">
    <style>
        body{
            font-family: 'Philosopher', sans-serif !important;
        }    
    </style> 
    
    {{-- Font-Awesome CSS --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"> 
    
    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="{{ asset('login-signup/css/bootstrap.css')}}"> 
    
    {{-- Custom CSS --}}
    <link rel="stylesheet" href="{{ asset('login-signup/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('login-signup/css/responsive.css')}}">
</head>

<body>
{{-- =======================< Login Part Starts>=============== --}}
<section class="login-page-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11">
                <div class="login-part">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="login-left d-sm-none d-none d-md-block">

                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="login-right">
                                <div class="row align-items-center">
                                    <div class="col-md-12">
                                        <div class="login-form">
                                            <form method="POST" action="{{ route('admin.password.email') }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required>

                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div>
                                                    <button class="btn btn-info btn-lg login-button" type="submit">
                                                        {{ __('Send Password Reset Link') }}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =======================< Login Part Starts>=============== --}} 




{{-- ====================Script Files========================= --}}
    <script src="{{ asset('login-signup/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ asset('login-signup/js/popper.min.js')}}"></script>
    <script src="{{ asset('login-signup/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('login-signup/js/script.js')}}"></script>
{{-- ==================Script Files=========================== --}}
</body>

</html>
