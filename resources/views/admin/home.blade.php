@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Total Bus</h4>
                    </div>
                    <div class="card-body">
                        <h2 class="color_ff text-center">
                            {{ \App\AllBus::all()->count('id') }}
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Total Driver</h4>
                    </div>
                    <div class="card-body">
                        <h2 class="color_ff text-center">
                            {{ \App\AllDriver::all()->count('id') }}
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Total Student</h4>
                    </div>
                    <div class="card-body">
                        <h2 class="color_ff text-center">
                            {{ \App\User::all()->count('id') }}
                        </h2>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Total Teacher</h4>
                    </div>
                    <div class="card-body">
                        <h2 class="color_ff text-center">
                            {{ \App\Teacher::all()->count('id') }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="row m-t-30">
                <div class="col-md-12 admin-dashboard">
                    <div class="card">
                        <div class="card-header">
                            <h4>All Routes</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="owl-carousel owl-theme" id="admin-routes">
                                    @foreach($routes as $route)
                                    <?php $i = 0; ?>
                                        <div class="item">
                                            <div class="card">
                                                <div class="card-header">
                                                
                                                    <h4 class="color_ff">Route-{{ $route->routeNo }}</h4>
                                                </div>
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-dark">
                                                            <thead>
                                                                
                                                                <tr>
                                                                    <th scope="col">Pick Point</th>
                                                                    <th scope="col">Total Student</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach (explode(',', $route->stoppagePoint) as $stoppagePoint)
                                                                <tr>
                                                                    <td class="fw_700">{{ trim($stoppagePoint) }}</td>   
                                                                    <td>{{ App\User::where(['pickPoint' => trim($stoppagePoint), 'routeNo' => $route->routeNo])->count() }}</td>
                                                                    <?php
                                                                        $count = App\User::where(['pickPoint' => trim($stoppagePoint), 'routeNo' => $route->routeNo])->count(); 
                                                                        $i += $count; 
                                                                    ?>
                                                                    <td>
                                                                        <a href="#" class="btn btn-info custom-btn btn-sm">Details</a>
                                                                    </td>
                                                                    
                                                                </tr>
                                                                @endforeach
    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <h5 class="color_ff">Total Students: <span class="float-right">{{ $i }}</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        

        <div class="row m-t-30">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h4>Bus Schedules <span class="float-right">Today</span> </h4> --}}
                        <h4>@if(isset($todayClass[0]->day)){{ $todayClass[0]->day }}@endif Bus Schedules</h4>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="owl-carousel owl-theme" id="bus-Schedule">
                                    @foreach ($todayBus as $bus)
                                    <div class="item">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="color_ff bustime">Time: <span class="float-right">{{ $bus->busTime }}</span> </h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-dark">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Route No</th>
                                                                <th scope="col">Total Student</th>
                                                                <th scope="col">Needed Bus</th>
                                                                {{-- <th scope="col">Extra Student</th> --}}
                                                                {{-- <th scope="col">Extra Bus</th> --}}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $routes = App\AllRoute::select('routeNo')->get(); ?>
                                                        @foreach ($routes as $route)
                                                            
                                                        
                                                            <tr>
                                                                <td class="fw_700">{{ $route->routeNo }}</td>
                                                                <?php 
                                                                    $countStudent = 0;
                                                                    foreach($todayClass as $today){
                                                                        if($today->busTime == $bus->busTime){
                                                                            $temp = App\User::where('studentBatch', $today->batch)
                                                                                        ->where('studentSection', $today->section)
                                                                                        ->where('routeNo', $route->routeNo)
                                                                                        ->count();
                                                                            $countStudent += $temp;
                                                                        }
                                                                    }
                                                                    
                                                                    $needBus = null;
                                                                    if($countStudent > 10 && $countStudent < 30){
                                                                        $needBus = 1;
                                                                    } else{
                                                                    $needBus = floor($countStudent/30);
                                                                    $needBus += ($countStudent%30 >= 15 ) ? 1:0;
                                                                    }

                                                                ?>
                                                                <td>{{ $countStudent }}</td>
                                                                <td>{{ $needBus }}</td>
                                                                {{-- <td>45</td> --}}
                                                                {{-- <td>00</td> --}}
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {{-- <div class="card-footer">
                                                <h5 class="color_ff">Need Extra Bus: <span class="float-right">02</span></h5>
                                            </div> --}}
                                        </div>
                                    </div>
                                 @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row m-t-30">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h4>Bus Schedules <span class="float-right">Tomorrow</span> </h4> --}}
                        <h4>@if(isset($tomorrowClass[0]->day)){{ $tomorrowClass[0]->day }}@endif Bus Schedules</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="owl-carousel owl-theme" id="bus-Schedule2">
                                    @foreach ($todayBus as $bus)
                                    <div class="item">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="color_ff">Time: <span class="float-right">{{ $bus->busTime }}</span> </h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-dark">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Route No</th>
                                                                <th scope="col">Total Student</th>
                                                                <th scope="col">Total Bus</th>
                                                                {{-- <th scope="col">Extra Student</th> --}}
                                                                {{-- <th scope="col">Extra Bus</th> --}}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $routes = App\AllRoute::select('routeNo')->get(); ?>
                                                        @foreach ($routes as $route)
                                                            <tr>
                                                                <td class="fw_700">{{ $route->routeNo }}</td>
                                                                <?php 
                                                                    $countStudent = 0;
                                                                    foreach($tomorrowClass as $Tomorrow){
                                                                        if($Tomorrow->busTime == $bus->busTime){
                                                                            $temp = App\User::where('studentBatch', $Tomorrow->batch)
                                                                                        ->where('studentSection', $Tomorrow->section)
                                                                                        ->where('routeNo', $route->routeNo)
                                                                                        ->count();
                                                                            $countStudent += $temp;
                                                                        }
                                                                    }
                                                                    $needBus = null;
                                                                    if($countStudent > 10 && $countStudent < 30){
                                                                        $needBus = 1;
                                                                    } else{
                                                                    $needBus = floor($countStudent/30);
                                                                    $needBus += ($countStudent%30 >= 15 ) ? 1:0;
                                                                    }
                                                                ?>
                                                                <td>{{ $countStudent }}</td>
                                                                <td>{{ $needBus }}</td>
                                                                {{-- <td>45</td> --}}
                                                                {{-- <td>00</td> --}}
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {{-- <div class="card-footer">
                                                <h5 class="color_ff">Need Extra Bus: <span class="float-right">02</span></h5>
                                            </div> --}}
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('scripts')


<script src="{{ asset('admin-css/js/momment.js')}}"></script>

<script>
    $(document).ready(function(){

        $.ajax({
            type:"GET",
            url:'{!! URL::to('admin/home/getRefuse') !!}',
            dataType:"JSON",
            success:function(data){


                for (var i=0; i< data.length; i++) { 

                    var today = new Date();
                    var cD = today.getDate();
                    var cM = today.getMonth() + 1; //January is 0!
                    var cY = today.getFullYear();

                    var rY = Number(data[i].refuse.substring(0, 4));
                    var rM = Number(data[i].refuse.substring(5, 7));
                    var rD = Number(data[i].refuse.substring(8, 10));
                    today = new Date(cY,cM,cD);
                    var rDay = new Date(rY, rM, rD);
                    var id = data[i].id;
                    if( Date.parse(today) > Date.parse(rDay) ){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type:"POST",
                            url:'{!! URL::to('/admin/home/refuse') !!}',
                            data:{'id': id},
                            dataType:"JSON",
                            success:function(data){
                            },
                            error:function(){
                                alert('Did not change data');
                            }
                        });
                    }
                    
                }
            },
            error:function(){
                alert('Data Not Found');
            }
        });

    });
</script>

<script>
$(document).ready(function(){
    // let startPosition = moment().isAfter(moment('10:20am', 'h:mma')) ? 1 : moment().isAfter(moment('11:45am', 'h:mma')) ? 3 : 2
  // let startPosition = moment().isAfter(moment('12:45pm', 'h:mma')) ? 0 : moment().isAfter(moment('11:45am', 'h:mma')) ? 4 : moment().isAfter(moment('10:30am', 'h:mma')) ? 3 : moment().isAfter(moment('09:45am', 'h:mma')) ? 2 : 0
  let startPosition = 0;
  if( moment().isAfter(moment('12:45pm', 'h:mma')) ){
    startPosition = 0;
  } else if( moment().isAfter(moment('11:45am', 'h:mma')) ){
    startPosition = 4;
  } else if( moment().isAfter(moment('10:30am', 'h:mma')) ){
    startPosition = 3;
  } else if( moment().isAfter(moment('09:45am', 'h:mma')) ){
    startPosition = 2;
  }  else if( moment().isAfter(moment('08:30am', 'h:mma')) ){
    startPosition = 1;
  }

  $('#bus-Schedule, #bus-Schedule2').owlCarousel({
    loop: true,
    margin: 10,
    autoHeight:true,
    nav: true,
    startPosition: startPosition,
    dots: false,
    // autoplay: true,
    navText: ['<i class="fa fa-2x fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-2x fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 2
      }
    }
  })
});
</script>
    
@endsection
