@extends('layouts.app-admin')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Admin Portal</li>
                        <li class="breadcrumb-item"><a href="{{ Route('alldepartments') }}">All Departments</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $alldepartment->deptShortName }}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="show-page">
                    <form class="fire_input2" method="post" action="{{route('alldepartment.update',['id'=>$alldepartment->id])}}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>{{ $alldepartment->deptShortName }}</h4>
                            </div>
                            <div class="back-page float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Department </label>
                                            <input type="text" value="{{ $alldepartment->deptShortName }}" name="deptShortName" class="input1 removeDis {{ $errors->has('deptShortName') ? ' is-invalid' : '' }}" disabled required>
                                            @if ($errors->has('deptShortName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('deptShortName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="group">
                                            <label>Department Full Name </label>
                                            <input type="text" value="{{ $alldepartment->departmentName }}" name="departmentName" class="input1 removeDis {{ $errors->has('departmentName') ? ' is-invalid' : '' }}" disabled required>
                                            @if ($errors->has('departmentName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('departmentName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-info custom-btn" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                        <button type="button" class="btn btn-info custom-btn hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>

                                        <button type="submit" class="btn btn-info custom-btn hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection