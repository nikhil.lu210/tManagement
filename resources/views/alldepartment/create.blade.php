@extends('layouts.app-admin')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Admin Portal</li>
                        <li class="breadcrumb-item"><a href="{{ Route('alldepartments') }}">All Departments</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Department</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-driver">
                    <form class="fire_input2" method="post" action="{{ Route('alldepartment.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Driver</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ route('alldepartments') }}" class="btn btn-info custom-btn"><i class="far fa-eye"></i></i> View Departments</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Department Short Name </label>
                                            <input type="text" placeholder="ex: CSE" name="deptShortName" class="input1 {{ $errors->has('deptShortName') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('deptShortName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('deptShortName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="group">
                                            <label>Department Full Name </label>
                                            <input type="text" placeholder="ex: Computer Science & Engineering" name="departmentName" class="input1 {{ $errors->has('departmentName') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('departmentName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('departmentName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Add Department</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection