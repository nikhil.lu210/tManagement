@extends('layouts.app-admin')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Admin Portal</li>
                        <li class="breadcrumb-item active" aria-current="page">All Departments</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="all-department">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>All Departments</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('alldepartment.create') }}" class="btn btn-info custom-btn"> Add Department</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <div class="table-search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-dark">
                                            <thead>
                                            <tr>
                                                <th scope="col">Department</th>
                                                <th scope="col">Department Title</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($alldepartments as $alldepartment)
                                                <tr>
                                                    <td>{{ $alldepartment->deptShortName }}</td>
                                                    <td>{{ $alldepartment->departmentName }}</td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <a href="{{ route('alldepartment.destroy', ['id'=>$alldepartment->id])}}" class="btn btn-info custom-btn btn-sm" onclick="return confirm('Are you sure to delete this item?')">
                                                                <i class="fas fa-trash"></i> 
                                                                Delete
                                                            </a>
                
                                                            <a href="{{ route('alldepartment.show', ['id'=>$alldepartment->id])}}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection