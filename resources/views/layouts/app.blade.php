<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--  <!-- CSRF Token -->  --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Transportation Management&nbsp; | &nbsp;ADMIN</title>

    <link rel="shortcut icon" href="{{ asset('files/images/logo.png')}}">

    {{--  <!-- Fonts -->  --}}
    {{--  <link rel="dns-prefetch" href="https://fonts.gstatic.com">  --}}
    {{--  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">  --}}
    <link rel="stylesheet" href="{{ asset('files\css\raleway-font.css') }}">

    {{--  Font-Awesome Icons  --}}
    {{--  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">  --}}
    {{--  <link href="{{ asset('fontawesome/css/fontawesome.min.css') }}" type="text/css" rel="stylesheet">   --}}
    <link href="{{ asset('fontawesome/css/all.css') }}" type="text/css" rel="stylesheet"> 


    <link href="{{ asset('admin-css/css/jquery-ui.css') }}" type="text/css" rel="stylesheet"> 
    
    {{-- input css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/input.css')}}"> 
    
    {{-- nice-select css --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select.css')}}"> 
    
    {{-- global style css --}}
    <link rel="stylesheet" href="{{ asset('files/css/global-style.css')}}"> 
    
    {{-- toastr css --}}
    <link rel="stylesheet" href="{{ asset('admin-css/css/toastr.min.css')}}"> 
    
    {{-- date-picker css --}}
    <link rel="stylesheet" href="{{ asset('files/css/datepicker.min.css')}}"> 

    {{-- owl-carousel css --}}
    <link rel="stylesheet" href="{{ asset('admin-css/css/owl/owl.carousel.min.css')}}"> 
    <link rel="stylesheet" href="{{ asset('admin-css/css/owl/owl.theme.default.min.css')}}"> 

    {{-- admin custom css --}}
    <link rel="stylesheet" href="{{ asset('admin-css/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('admin-css/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('admin-css/css/responsive.css')}}">

    {{-- files custom css --}}
    <link rel="stylesheet" href="{{ asset('files/css/margin-padding.css')}}"> 
    <link rel="stylesheet" href="{{ asset('files/css/style.css')}}"> 
</head>

<body class="app sidebar-mini rtl">
    <div id="app">
        <header class="app-header">
            <a class="app-header__logo" href="{{ url('/home') }}">
                {{--  <img src="{{ asset('files/images/logo.png') }}" alt="Logo" style="max-width: 140px;">  --}}
                <h4 class="m-t-10">LU TRANSPORT</h4>
            </a>
            <!-- Sidebar toggle button-->
            <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"><i class="fas fa-bars"></i></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">
                @guest
                <li><a class="" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                <li><a class="" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                @else
                {{--  User Menu  --}}
                <li class="dropdown">
                    <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fas fa-user p-r-5"></i> 
                        {{ Auth::user()->name }}
                    </a>
                    
                    <ul class="dropdown-menu settings-menu dropdown-menu-right">
                        
                        <li><a class="dropdown-item" href="{{ route('myprofiles') }}"><i class="fas fa-user fw_300 p-r-5"></i> My Profile</a></li>

                        <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fas fa-power-off p-r-5"></i>Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest
            </ul>
        </header>
        
        {{--  <!-- Sidebar menu-->  --}}
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="app-sidebar">
            <div class="app-sidebar__user">
                @if (isset(Auth::user()->avatar))
                    <img class="app-sidebar__user-avatar" src="{{ asset('images/'.Auth::user()->avatar) }}" alt="User Image">
                @else
                    <img class="app-sidebar__user-avatar" src="{{ asset('admin-css/images/student.png') }}" alt="User Image">
                @endif
                
                <div>
                    <p class="app-sidebar__user-name">Transportation</p>
                    <p class="app-sidebar__user-designation">{{ Auth::user()->name }}</p>
                </div>
            </div>
            <ul class="app-menu">
                {{-- Dashboard --}}
                <li>
                    <a class="app-menu__item" href="{{ url('/home') }}">
                        <i class="fas fa-home p-r-5"></i>
                        <span class="app-menu__label">Student Dashboard</span>
                    </a>
                </li>

                {{-- Student Portal --}}
                {{--  <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-graduation-cap"></i><span class="app-menu__label">Student Portal</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">
                        
                        <li><a class="treeview-item" href="{{ route('regularclassschedules') }}"><i class="far fa-hand-point-right p-r-5"></i> Regular Class Schedule</a></li>
                        
                        <li><a class="treeview-item" href="{{ route('extraclassschedules') }}"><i class="far fa-hand-point-right p-r-5"></i> Extra Class Schedule</a></li>
                    </ul>
                </li>  --}}

                {{-- Settings --}}
                <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-cog"></i><span class="app-menu__label">Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">

                        {{--  Students Profile  --}}
                        <li><a class="treeview-item" href="{{ route('myprofiles') }}"><i class="fas fa-user fw_300 p-r-5"></i> My Profile</a></li>

                        {{--  Students Route  --}}
                        <li><a class="treeview-item" href="{{ route('myroutes') }}"><i class="fas fa-street-view p-r-5"></i> My Route</a></li>
                    </ul>
                </li>
            </ul>
        </aside>
        <main class="app-content">

            @yield('content')
        </main>
    </div>

    {{--  <!-- Scripts -->  --}}
    <script src="{{ asset('admin-css/js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('admin-css/js/popper.min.js') }}" defer></script>
    {{--
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script> --}}
    <script src="{{ asset('admin-css/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin-css/js/bootstrap.min.js') }}" defer></script>

    {{-- nice-select js --}}
    <script src="{{ asset('files/js/jquery.nice-select.min.js')}}"></script>

    {{-- toastr js --}}
    <script src="{{ asset('admin-css/js/toastr.min.js')}}"></script>

    {{-- fire-ui js --}}
    <script src="{{ asset('files/js/main.js')}}"></script>

    {{--  ===========owl-carousel js==========  --}}
    <script src="{{ asset('admin-css/js/owl/owl.carousel.min.js')}}"></script>

    {{-- date-picker js --}}
    <script src="{{ asset('files/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('files/js/datepicker.en.js')}}"></script>

    {{--
    <!-- =========New JS Files======== -->--}}
    <script src="{{ asset('admin-css/js/main.js')}}"></script>
    <script src="{{ asset('admin-css/js/script.js')}}"></script>
    {{-- <script>
        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(Session::has('error'))
        toastr.error("{{Session::get('error')}}");
        @endif
    </script> --}}
    <script>
        window.FontAwesomeConfig = {
            searchPseudoElements: true
        }
    </script>

    <script>

        $(document).ready(function(){

            $(document).on('change','.selectRouteNo',function(){
                
                var route_id=$(this).val();
                var div=$(this).parent().parent().parent();

                var op=" ";

                $.ajax({
                    type:"GET",
                    url:'{!! URL::to('admin/allteacher/getRoute') !!}',
                    data:{'id':route_id},
                    dataType:"JSON",
                    success:function(data){
                        //alert('Data Found');

                        var strArray = data[0].stoppagePoint.split(",");
                        
                        op+='<option value=" " selected disabled>Select Your Pickpoint</option>';
                        
                        for(var i=0;i<strArray.length;i++){
                            op+='<option value="'+strArray[i]+'">'+strArray[i]+'</option>';
                            //op+='<li data-value="'+strArray[i]+'" class="option">'+strArray[i]+'</li>';
                        }                

                        div.find('.stoppagePoint').html(" ");
                        div.find('.stoppagePoint').append(op);
                    },
                    error:function(){
                        alert('Data Not Found');
                    }
                });
            });

            
        });

    </script>

    <script>
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
    </script>
    
</body>

</html>