<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Transportation Management&nbsp; | &nbsp;ADMIN</title>

    <link rel="shortcut icon" href="{{ asset('files/images/logo.png')}}">

    {{--  <!-- Fonts -->  --}}
    {{--  <link rel="dns-prefetch" href="https://fonts.gstatic.com">  --}}
    {{--  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">  --}}
    <link rel="stylesheet" href="{{ asset('files\css\raleway-font.css') }}">

    {{--  Font-Awesome Icons  --}}
    {{--  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">  --}}
    {{--  <link href="{{ asset('fontawesome/css/fontawesome.min.css') }}" type="text/css" rel="stylesheet">   --}}
    <link href="{{ asset('fontawesome/css/all.css') }}" type="text/css" rel="stylesheet"> 

    <link href="{{ asset('admin-css/css/jquery-ui.css') }}" type="text/css" rel="stylesheet" /> 
    
    {{-- input css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/input.css')}}"> 
    
    {{-- nice-select css --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select.css')}}"> 
    
    {{-- global style css --}}
    <link rel="stylesheet" href="{{ asset('files/css/global-style.css')}}"> 
    
    {{-- toastr css --}}
    <link rel="stylesheet" href="{{ asset('admin-css/css/toastr.min.css')}}"> 
    
    {{-- owl-carousel css --}}
    <link rel="stylesheet" href="{{ asset('admin-css/css/owl/owl.carousel.min.css')}}"> 
    <link rel="stylesheet" href="{{ asset('admin-css/css/owl/owl.theme.default.min.css')}}"> 
    
    {{-- date-picker css --}}
    <link rel="stylesheet" href="{{ asset('files/css/datepicker.min.css')}}"> 
    
    {{-- files custom css --}}
    <link rel="stylesheet" href="{{ asset('files/css/margin-padding.css')}}"> 
    <link rel="stylesheet" href="{{ asset('files/css/style.css')}}"> 
    
    {{-- admin custom css --}}
    <link rel="stylesheet" href="{{ asset('admin-css/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('admin-css/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('admin-css/css/responsive.css')}}">
</head>

<body class="app sidebar-mini rtl">
    <div id="app">
        <header class="app-header">
            <a class="app-header__logo" href="{{ url('admin/home') }}">
                {{--  <img src="{{ asset('files/images/logo.png') }}" alt="Logo" style="max-width: 140px;">  --}}
                <h4 class="m-t-10">LU TRANSPORT</h4>
            </a>
            <!-- Sidebar toggle button-->
            <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"><i class="fas fa-bars"></i></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">
                {{-- @guest
                <li><a class="" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                <li><a class="" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                @else --}}
                <!-- User Menu-->
                <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg fw_300 p-r-5"></i> {{ Auth()->guard('admin')->user()->name }}</a>
                    <ul class="dropdown-menu settings-menu dropdown-menu-right">
                        {{--  <li><a class="dropdown-item" href="#"><i class="fa fa-cog fa-lg"></i> Notification <sup style="color: white; font-weight: bold;">02</sup> </a></li>  --}}
                        <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fas fa-power-off p-r-5"></i>Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                {{-- @endguest --}}
            </ul>
        </header>
        
        {{--  <!-- Sidebar menu-->  --}}
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="app-sidebar">
            <div class="app-sidebar__user">
                <img class="app-sidebar__user-avatar" src="{{ asset('admin-css/images/admin.png') }}" alt="User Image">
                <div>
                    <p class="app-sidebar__user-name">Transportation</p>
                    <p class="app-sidebar__user-designation">{{ Auth()->guard('admin')->user()->name }}</p>
                </div>
            </div>
            <ul class="app-menu">
                {{-- Dashboard --}}
                <li>
                    <a class="app-menu__item" href="{{ url('admin/home') }}">
                        <i class="app-menu__icon fab fa-bitbucket"></i>
                        <span class="app-menu__label">Admin Dashboard</span>
                    </a>
                </li>

                {{-- Student & Teacher Portal --}}
                <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-graduation-cap"></i><span class="app-menu__label">Student & Teacher</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">
                        
                        <li><a class="treeview-item" href="{{ Route('allstudents') }}"><i class="fas fa-graduation-cap p-r-5"></i> All Students</a></li>

                        <li><a class="treeview-item" href="{{ Route('allteachers') }}"><i class="fas fa-users p-r-5"></i> All Teachers</a></li>
                    </ul>
                </li>

                
                {{-- Admin Portal --}}
                <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-user"></i><span class="app-menu__label">Admin Portal</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">

                        <li><a class="treeview-item" href="{{ Route('alldepartments') }}"><i class="far fa-building p-r-5"></i> All Departments</a></li>

                        <li><a class="treeview-item" href="{{ Route('allbatches') }}"><i class="fab fa-quinscape p-r-5"></i> All Batches</a></li>

                        <li><a class="treeview-item" href="{{ Route('allroutes') }}"><i class="fas fa-sitemap p-r-5"></i> All Routes</a></li>

                        <li><a class="treeview-item" href="{{ Route('allbuses') }}"><i class="fas fa-bus p-r-5"></i> All Buses</a></li>

                        <li><a class="treeview-item" href="{{ Route('alldrivers') }}"><i class="fas fa-users p-r-5"></i> All Drivers</a></li>

                    </ul>
                </li>

                {{-- Settings --}}
                <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-cog"></i><span class="app-menu__label">Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">

                        <li><a class="treeview-item" href="{{ Route('allbus.create') }}"><i class="fas fa-bus p-r-5"></i> Add Bus</a></li>

                        <li><a class="treeview-item" href="{{ Route('alldriver.create') }}"><i class="fas fa-user-plus p-r-5"></i> Add Driver</a></li>

                        <li><a class="treeview-item" href="{{ Route('allroute.create') }}"><i class="fas fa-map p-r-5"></i> Add Routes</a></li>
                        
                        <li><a class="treeview-item" href="{{ Route('routines') }}"><i class="fas fa-cloud-upload-alt p-r-5"></i> Routine</a></li>

                    </ul>
                </li>
            </ul>
        </aside>
        <main class="app-content">

            @yield('content')
        </main>
    </div>

    {{--  <!-- Scripts -->  --}}
    <script src="{{ asset('admin-css/js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('admin-css/js/popper.min.js') }}" defer></script>
    {{--
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script> --}}
    <script src="{{ asset('admin-css/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin-css/js/bootstrap.min.js') }}" defer></script>

    {{-- nice-select js --}}
    {{-- <script src="{{ asset('files/js/jquery.nice-select.min.js')}}"></script> --}}

    {{-- toastr js --}}
    <script src="{{ asset('admin-css/js/toastr.min.js')}}"></script>

    {{-- fire-ui js --}}
    <script src="{{ asset('files/js/main.js')}}"></script>

    {{-- date-picker js --}}
    <script src="{{ asset('files/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('files/js/datepicker.en.js')}}"></script>

    {{--  ===========Nice Scroll js==========  --}}
    <script src="{{ asset('admin-css/js/jquery.nicescroll.min.js')}}"></script>

    {{--  ===========owl-carousel js==========  --}}
    <script src="{{ asset('admin-css/js/owl/owl.carousel.min.js')}}"></script>

    {{--  ===========Toaster JS=========  --}}
    <script>
        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(Session::has('error'))
        toastr.error("{{Session::get('error')}}");
        @endif
    </script>

    {{--  ===========Ajax Function=========  --}}
    <script>

        $(document).ready(function(){

            $(document).on('change','.selectRouteNo',function(){
                
                var route_id=$(this).val();
                var div=$(this).parent().parent().parent();

                var op=" ";

                $.ajax({
                    type:"GET",
                    url:'{!! URL::to('admin/allteacher/getRoute') !!}',
                    data:{'id':route_id},
                    dataType:"JSON",
                    success:function(data){
                        //alert('Data Found');

                        var strArray = data[0].stoppagePoint.split(",");
                        
                        op+='<option value=" " selected disabled>Select Your Pickpoint</option>';
                        
                        for(var i=0;i<strArray.length;i++){
                            op+='<option value="'+strArray[i]+'">'+strArray[i]+'</option>';
                            //op+='<li data-value="'+strArray[i]+'" class="option">'+strArray[i]+'</li>';
                        }                

                        div.find('.stoppagePoint').html(" ");
                        div.find('.stoppagePoint').append(op);
                    },
                    error:function(){
                        alert('Data Not Found');
                    }
                });
            });

            
        });


        /*$('#selectRouteNo').change(function(){
            var route_no = $('#selectRouteNo').val();
            var url_name = '/admin/allteacher/create/' + route_no;

            $.get(url_name, function(response) {
                var strArray = response[0].stoppagePoint.split(",");
                
                var len = strArray.length;
                var c = $('#selectPickPoint').children('option:not(:first)').remove();

                for(var i = 0; i < len; i++){
                    var b = strArray[i].trim();
                    success:function(data){
            
                        op+='<option value="0" selected disabled>Select Pick Point</option>';
                        for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].id+'">'+data[i].stoppagePoint+'</option>';
                    }
                    

                    div.find('.stoppagePoint').html(" ");
                    div.find('.stoppagePoint').append(op);
                    },
                }
            });
        });*/
    </script>


    {{-- =========New JS Files======== --}}
    <script src="{{ asset('admin-css/js/main.js')}}"></script>
    @yield('scripts')
    <script src="{{ asset('admin-css/js/script.js')}}"></script>

    
</body>

</html>