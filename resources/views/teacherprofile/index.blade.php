@extends('layouts.app-teacher')

@section('content')

<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('teacher/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">My Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>The Profile Of Teacher</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="user-image">
                                    @if (isset(Auth()->guard('teacher')->user()->avatar))
                                        <img src="{{ asset('images/'.$teacher->avatar) }}" alt="Profile Picture Not Found" class="img-responsive img-thumbnail rounded float-left" style="max-width:100%;" />
                                    @else
                                        <img src="{{ asset('admin-css/images/teacher.png') }}" alt="Profile Picture Not Found" class="img-responsive img-thumbnail rounded float-left" style="max-width:100%;" />
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="user-details">
                                    <table class="table table-bordered table-dark">

                                        <tbody>

                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Name:</th>
                                                <td style="text-align: left !important;">{{ $teacher->name }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Department:</th>
                                                <td style="text-align: left !important;">{{ $teacher->teacherDepartment }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Route No:</th>
                                                <td style="text-align: left !important;">{{ $teacher->routeNo }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Pick Point:</th>
                                                <td style="text-align: left !important;">{{ $teacher->pickPoint }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Mobile No:</th>
                                                <td style="text-align: left !important;">{{ $teacher->teacherNumber }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Email:</th>
                                                <td style="text-align: left !important;"> {{ $teacher->email }} </td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Address:</th>
                                                <td style="text-align: left !important;">
                                                    {{ $teacher->address }}
                                                </td>
                                            </tr>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ Route('teacherprofile.create') }}" class="btn btn-info custom-btn btn-sm float-right"><i class="fas fa-pencil-alt"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
