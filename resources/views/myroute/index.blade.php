@extends('layouts.app')

@section('content')
<section class="student-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">My Route</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ Auth::user()->name }} Bus Route</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header text-center">
                                        <h4 class="color_ff">Route No</h4>
                                    </div>
                                    <div class="card-body text-center">
                                        <h1 class="color_ff">01</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header text-center">
                                        <h4 class="color_ff">Pick Point</h4>
                                    </div>
                                    <div class="card-body text-center">
                                        <h1 class="color_ff">Surma Tower</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header text-center">
                                        <h4 class="color_ff">Drop Point</h4>
                                    </div>
                                    <div class="card-body text-center">
                                        <h1 class="color_ff">Subidbazar</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-info custom-btn btn-sm float-right"><i class="far fa-thumbs-up"></i> Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
