@extends('layouts.app-admin')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">Add Driver</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-driver">
                    <form class="fire_input2" method="post" action="{{ Route('alldriver.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Driver</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ route('alldrivers') }}" class="btn btn-info custom-btn"><i class="far fa-eye"></i></i> View Drivers</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>ID. </label>
                                            <input type="text" placeholder="ex: DRIVER18201" name="driverID" class="input1 {{ $errors->has('driverID') ? ' is-invalid' : '' }}" value="LUDRVR" required>
                                            @if ($errors->has('driverID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverID') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Full Name </label>
                                            <input type="text" placeholder="ex: Md. Rahel Miah" name="driverName" class="input1 {{ $errors->has('driverName') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('driverName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Mobile No</label>
                                            <input type="text" placeholder="ex: 01712345678" name="driverMobile" class="input1 {{ $errors->has('driverMobile') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('driverMobile'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverMobile') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Driving Licence No</label>
                                            <input type="text" placeholder="ex: DRVLNS-2342734" name="driverLicence" class="input1 {{ $errors->has('driverLicence') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('driverLicence'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverLicence') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>NID No</label>
                                            <input type="text" placeholder="ex: 19951245653167121" name="driverNID" class="input1 {{ $errors->has('driverNID') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('driverNID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverNID') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Identity Photo</label>
                                            <input type="file" placeholder="ex: Photo" name="driverPhoto" class="input1 {{ $errors->has('driverPhoto') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('driverPhoto'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverPhoto') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Driver Route </label>
                                            <select name="driverRoute" id="" class="{{ $errors->has('driverRoute') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Route No.</option>
                                                @foreach($allRoutes as $route)
                                                    <option value="{{ $route->routeNo }}">{{ $route->routeNo }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('driverRoute'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverRoute') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="group">
                                            <label>Full Address </label>
                                            <input type="text" placeholder="ex: Ragibnagar, Kamalbazar, KB-301/B" name="driverAddress" class="input1 {{ $errors->has('driverAddress') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('driverAddress'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverAddress') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Add Driver</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
