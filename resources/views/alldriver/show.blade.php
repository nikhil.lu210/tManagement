@extends('layouts.app-admin') @section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Settings</li>
                        <li class="breadcrumb-item"><a href="{{ Route('alldrivers') }}">All Drivers</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $alldriver->driverName }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-driver">
                    <form class="fire_input2" method="post" action="{{route('alldriver.update',['id'=>$alldriver->id])}}">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <div class="heading float-left">
                                    <h4>{{ $alldriver->driverName }}</h4>
                                </div>
                                <div class="back-page float-right">
                                    <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-custom-background">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>ID. </label>
                                                <input type="text" value="{{ $alldriver->driverID }}" name="driverID" class="input1 removeDis {{ $errors->has('driverID') ? ' is-invalid' : '' }}" disabled required> @if ($errors->has('driverID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverID') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Full Name </label>
                                                <input type="text" value="{{ $alldriver->driverName }}" name="driverName" class="input1 removeDis {{ $errors->has('driverName') ? ' is-invalid' : '' }}" disabled required> @if ($errors->has('driverName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverName') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Mobile No</label>
                                                <input type="text" value="{{ $alldriver->driverMobile }}" name="driverMobile" class="input1 removeDis {{ $errors->has('driverMobile') ? ' is-invalid' : '' }}" disabled required> @if ($errors->has('driverMobile'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverMobile') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Driving Licence No</label>
                                                <input type="text" value="{{ $alldriver->driverLicence }}" name="driverLicence" class="input1 removeDis {{ $errors->has('driverLicence') ? ' is-invalid' : '' }}" disabled required> @if ($errors->has('driverLicence'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverLicence') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>NID No</label>
                                                <input type="text" value="{{ $alldriver->driverNID }}" name="driverNID" class="input1 removeDis {{ $errors->has('driverNID') ? ' is-invalid' : '' }}" disabled required> @if ($errors->has('driverNID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverNID') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Identity Photo</label>
                                                <input type="file" value="{{ $alldriver->driverPhoto }}" name="driverPhoto" class="input1 removeDis {{ $errors->has('driverPhoto') ? ' is-invalid' : '' }}" disabled> @if ($errors->has('driverPhoto'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverPhoto') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fire_select2 fire_select">
                                                <label>Route No. </label>
                                                <select name="routeNo" id="routeNo" class="removeDis {{ $errors->has('routeNo') ? ' is-invalid' : '' }}" disabled required>
                                                    <option value="{{$alldriver->driverRoute}}">{{$alldriver->driverRoute}}</option>
                                                    @foreach($allRoutes as $route)
                                                    <option value="{{ $route->routeNo }}">{{ $route->routeNo }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('routeNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routeNo') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="group">
                                                <label>Full Address </label>
                                                <input type="text" value="{{ $alldriver->driverAddress }}" name="driverAddress" class="input1 removeDis {{ $errors->has('driverAddress') ? ' is-invalid' : '' }}" disabled required> @if ($errors->has('driverAddress'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverAddress') }}</strong>
                                                </span> @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-info custom-btn" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                            <button type="button" class="btn btn-info custom-btn hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>

                                            <button type="submit" class="btn btn-info custom-btn hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection