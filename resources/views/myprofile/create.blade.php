@extends('layouts.app')

@section('content')
{{-- @if (Session::has('success'))
    <div class="row">
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
        <strong>Success: </strong> {{ Session::get('success') }}
        </div>
    </div>
    </div>
    
@endif

@if (Session::has('error'))
    <div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
        <strong>Error: </strong> {{ Session::get('error') }}
        </div>
    </div>
    </div>
    
@endif

@if (count($errors) > 0)

    <div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
        <strong>Errors: </strong> 
        <ul>
        @foreach($errors->all() as $error)
            <li> {{ $error }} </li>
        @endforeach
        </ul>
    </div>
    </div>
    </div>

    
@endif --}}
    
<section class="student-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Student Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-student">
                    <form enctype="multipart/form-data" class="fire_input2" method="post" action="{{ Route('myprofile.store') }}" role="form">
                    @csrf

                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Student</h4>
                            </div>
                            <div class="add-car float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="group">
                                            <div class="avatar-upload">
                                                <div class="avatar-edit">
                                                    <input type='file' name="avatar" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                    <label for="imageUpload"></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="imagePreview" @if($myprofile->avatar) style="background-image: url({{ asset('images/'.$myprofile->avatar) }});" @else style="background-image: url(http://i.pravatar.cc/500?img=7);" @endif>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Student ID. </label>
                                            <input type="text" name="studentID" class="input1" value="{{$myprofile->studentID}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Full Name </label>
                                            <input type="text" placeholder="ex: Jhon Doe" name="name" class="input1 {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $myprofile->name}}" required>
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Department </label>
                                            <input type="text"  name="studentDept" class="input1 " value="{{ $myprofile->studentDept}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Batch </label>
                                            <input type="text"  name="studentDept" class="input1 " value="{{ $myprofile->studentBatch}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Section </label>
                                            <input type="text"  name="studentSection" class="input1 " value="{{ $myprofile->studentSection}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fire_select2 fire_select">
                                            <label>Route No</label>
                                            <select name="routeNo" id="selectRouteNo" class="selectRouteNo {{ $errors->has('routeNo') ? ' is-invalid' : '' }}"  value="{{$myprofile->routeNo}}" required>
                                                <option value="">Select Route No.</option>
                                                @foreach($allRoutes as $route)
                                                    <option value="{{ $route->routeNo }}" @if($myprofile->routeNo == $route->routeNo) selected @endif>{{ $route->routeNo }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('routeNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routeNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fire_select2 fire_select">
                                            <label>Pick Point</label>
                                            <select name="pickPoint" id="selectPickPoint" class="stoppagePoint {{ $errors->has('pickPoint') ? ' is-invalid' : '' }}" required>
                                                <option value="">{{$myprofile->pickPoint}}</option>
                                                
                                            </select>
                                            @if ($errors->has('pickPoint'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('pickPoint') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Mobile No.</label>
                                            <input type="text" placeholder="ex: Jhon Doe" name="studentMobile" class="input1 {{ $errors->has('studentMobile') ? ' is-invalid' : '' }}" value="{{$myprofile->studentMobile}}" required>
                                            @if ($errors->has('studentMobile'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentMobile') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Parent Mobile No.</label>
                                            <input type="text" placeholder="ex: Jhon Doe" name="parentMobile" class="input1 {{ $errors->has('parentMobile') ? ' is-invalid' : '' }}" value="{{$myprofile->parentMobile}}" required>
                                            @if ($errors->has('parentMobile'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('parentMobile') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Email Address</label>
                                            <input type="text" placeholder="ex: Jhon Doe" name="email" class="input1 {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$myprofile->email}}" required>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="group">
                                            <label>Address</label>
                                            <textarea class="form-control" aria-label="With textarea" name="studentAddress" rows="3" required>{{$myprofile->studentAddress}}</textarea>

                                            {{-- <textarea type="text" placeholder="ex: Address" name="studentAddress" class="input1 {{ $errors->has('studentAddress') ? ' is-invalid' : '' }}" rows="3" value="{{$myprofile->studentAddress}}"required></textarea> --}}
                                            @if ($errors->has('studentAddress'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentAddress') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- ========< Change Password >======== --}}
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-student">
                <form class="fire_input2" method="post" action="{{ route('myprofile.changePassword') }}" role="form">
                    @csrf

                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Change Password</h4>
                            </div>
                            <div class="add-car float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Old Password </label>
                                            <input type="password" name="oldPassword" class="input1 {{ $errors->has('oldPassword') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('oldPassword'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('oldPassword') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>New Password </label>
                                            <input type="password" name="newPassword" class="input1 {{ $errors->has('newPassword') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('newPassword'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('newPassword') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label> Retype New Password </label>
                                            <input type="password" name="retypeNewPassword" class="input1 {{ $errors->has('retypeNewPassword') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('retypeNewPassword'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('retypeNewPassword') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Change Password</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

