@extends('layouts.app')

@section('content')
<section class="student-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">My Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>The Profile Of {{ Auth::user()->name }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="user-image">
                                    @if (isset($myprofile->avatar))
                                        <img src="{{ asset('images/'.$myprofile->avatar) }}" alt="Profile Picture Not Found" class="img-responsive img-thumbnail rounded float-left" style="max-width:100%;" />
                                    @else
                                        <img src="{{ asset('admin-css/images/student.png') }}" alt="Profile Picture Not Found" class="img-responsive img-thumbnail rounded float-left" style="max-width:100%;" />
                                    @endif
                                    
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="user-details">
                                    <table class="table table-bordered table-dark">
                                        {{--  <thead>
                                        <tr>
                                            <th scope="col">Route No</th>
                                            <th scope="col">Total Stoppage</th>
                                            <th scope="col">Total Student</th>
                                            <th scope="col">Total Teacher</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>  --}}
                                        <tbody>
                                        {{--  @foreach ($allroutes as $allroute)  --}}
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Name:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->name }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">ID:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->studentID }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Department:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->studentDept }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Batch:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->studentBatch }} <sup>( {{ $myprofile->studentSection }} )</sup></td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Route No:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->routeNo }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Pick Point:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->pickPoint }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Mobile No:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->studentMobile }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Parents No:</th>
                                                <td style="text-align: left !important;">{{ $myprofile->parentMobile }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Email:</th>
                                                <td style="text-align: left !important;"><a href="#">{{ $myprofile->email }}</a></td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Address:</th>
                                                <td style="text-align: left !important;">
                                                    {{ $myprofile->studentAddress }}
                                                </td>
                                            </tr>
                                        {{--  @endforeach  --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ Route('myprofile.create') }}" class="btn btn-info custom-btn btn-sm float-right"><i class="fas fa-pencil-alt"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
