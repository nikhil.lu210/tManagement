<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <title>LU Transportation | STUDENT LOGIN</title>

    {{-- favicon icon --}}
    <link rel="icon" href="{{ asset('login-signup/images/logo.png')}}"> 
    
    {{-- Google Font --}}
    <link href="https://fonts.googleapis.com/css?family=Philosopher:400,700" rel="stylesheet">
    <style>
        body{
            font-family: 'Philosopher', sans-serif !important;
        }    
    </style> 
    
    {{-- Font-Awesome CSS --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"> 
    <link rel="stylesheet" href="{{ asset('login-page/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}"> 
    <link rel="stylesheet" href="{{ asset('login-page/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}"> 
    
    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="{{ asset('login-page/vendor/bootstrap/css/bootstrap.min.css')}}"> 
    
    {{-- Custom CSS --}}
    <link rel="stylesheet" href="{{ asset('login-page/css/util.css')}}">
    <link rel="stylesheet" href="{{ asset('login-page/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('admin-css/css/style.css')}}">
</head>

<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(login-page/images/bg-01.jpg);">
					<span class="login100-form-title-1">
						S T U D E N T
					</span>
				</div>

				<form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                @csrf
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Student ID</span>
						<input class="input100 {{ $errors->has('studentID') ? ' is-invalid' : '' }}" type="text" name="studentID" placeholder="Enter studentID" required autofocus> 
                                                    
                        @if ($errors->has('studentID'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('studentID') }}</strong>
                            </span> 
                        @endif
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
                        <input class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Enter password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span> 
                        @endif
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}>
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="{{ route('password.request') }}" class="txt1">
								Forgot Password?
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
                        <button type="submit" class="btn btn-info login100-form-btn custom-btn" style="float:right;">
                            LOGIN
                        </button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	{{-- ====================Script Files========================= --}}
        <script src="{{ asset('login-page/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
        <script src="{{ asset('login-page/bootstrap/js/popper.js')}}"></script>
        <script src="{{ asset('login-page/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('login-page/js/main.js')}}"></script>
    {{-- ==================Script Files=========================== --}}

</body>
</html>