@extends('layouts.app-admin')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Admin Portal</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allbatches') }}">All Batches</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Batch</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-bus">
                    <form class="fire_input2" method="post" action="{{ Route('allbatch.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Batch</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('allbatches') }}" class="btn btn-info custom-btn"><i class="far fa-eye"></i></i> View Batches</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Department *</label>
                                            <select name="deptShortName" id="" class="{{ $errors->has('deptShortName') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Department No.</option>
                                                @foreach($allDepartments as $department)
                                                    <option value="{{ $department->deptShortName }}">{{ $department->deptShortName }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('deptShortName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('deptShortName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Batch No *</label>
                                            <input type="text" placeholder="ex: 38" name="batchNo" class="input1 {{ $errors->has('batchNo') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('batchNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('batchNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Section</label>
                                            <input type="text" placeholder="ex: E" name="batchSection" class="input1">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>ID Starts At *</label>
                                            <input type="number" placeholder="ex: 1512020201" name="startID" class="input1 {{ $errors->has('startID') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('startID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('startID') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>ID Ends At *</label>
                                            <input type="number" placeholder="ex: 1512020250" name="endID" class="input1 {{ $errors->has('endID') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('endID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('endID') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Password (min 6 character) *</label>
                                            <input type="text" placeholder="ex: abcdef123456" name="studentPassword" class="input1 {{ $errors->has('studentPassword') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('studentPassword'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentPassword') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn custom-btn"><i class="far fa-thumbs-up"></i> Add Batch</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection