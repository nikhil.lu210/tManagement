@extends('layouts.app-admin')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Admin Portal</li>
                        <li class="breadcrumb-item active" aria-current="page">All Batches</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="routine">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>All Batches</h4>
                            </div>
                            <div class="add-batch float-right">
                                <a href="{{ Route('allbatch.create') }}" class="btn btn-info custom-btn"> Add Batch</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <div class="table-search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-dark">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Department</th>
                                                    <th scope="col">Batch</th>
                                                    <th scope="col">Total Student</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($allbatches as $key=>$allbatch)
                                                
                                                <tr>
                                                    <td class="fw_700">{{ $allbatch->deptShortName }}</td>
                                                    <td>{{ $allbatch->batchNo }}</td>

                                                    <td>{{ $count[$key] }}</td>
                                                
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            {{--  <a href="#" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-trash"></i> 
                                                                Delete
                                                            </a>  --}}
                                
                                                            <a href="{{ route('allbatch.show', ['id' => $allbatch->batchNo,'deptName' => $allbatch->deptShortName]) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pagination float-right">
                                                {!! $allbatches->links(); !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection