@extends('layouts.app-admin')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Admin Portal</li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/allbatches') }}">All Batches</a></li>
                        {{-- <li class="breadcrumb-item active" aria-current="page">{{ $allbatch->batchNo }}</li> --}}
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="routine">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Batch No: </h4>
                            </div>
                            <div class="back-page float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <div class="table-search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-dark">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Department</th>
                                                    <th scope="col">Batch</th>
                                                    <th scope="col">Section</th>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Password</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        @foreach($allbatches as $allbatch)
                                                <tr>
                                                    <td>{{ $allbatch->deptShortName }}</td>
                                                    <td>{{ $allbatch->batchNo }}</td>
                                                    <td>{{ $allbatch->batchSection }}</td>
                                                    <td>{{ $allbatch->studentID }}</td>
                                                    <td>{{ $allbatch->studentPassword }}</td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                             <a href="{{ route('allbatch.destroy',['id' => $allbatch->id]) }}" onclick="return confirm('Are you sure to delete this item?')" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-trash"></i> 
                                                                Delete
                                                            </a> 
                                
                                                            <a href="#" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                        @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pagination float-left">
                                                {!! $allbatches->links(); !!}
                                            </div>
                                            <div class="float-right">
                                                <a class="btn btn-info custom-btn" onclick="window.print()" style="text-decoration: none; font-weight: bold;"><i class="fas fa-print"></i> Print</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection