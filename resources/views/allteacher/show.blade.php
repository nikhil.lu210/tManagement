@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Students & Teachers</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allteachers') }}">All Teachers</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $allteacher->teacherName }}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="card">
                <div class="card-header">
                    <h4 class="float-left">The Profile Of {{ $allteacher->name }}</h4>
                    <div class="back-page float-right">
                        <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="user-image">
                                <img src="{{ asset('images/'.$allteacher->avatar) }}" alt="Profile Picture Not Found" class="img-responsive img-thumbnail rounded float-left" style="max-width:100%;" />
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="user-details">
                                <table class="table table-bordered table-dark">
                                    <tbody>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Name:</th>
                                            <td style="text-align: left !important;">{{ $allteacher->name }}</td>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Department:</th>
                                            <td style="text-align: left !important;">{{ $allteacher->teacherDepartment }}</td>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Route No:</th>
                                            <td style="text-align: left !important;">{{ $allteacher->routeNo }}</td>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Pick Point:</th>
                                            <td style="text-align: left !important;">{{ $allteacher->pickPoint }}</td>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Mobile No:</th>
                                            <td style="text-align: left !important;">{{ $allteacher->teacherNumber }}</td>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Email:</th>
                                            <td style="text-align: left !important;">{{ $allteacher->email }}</td>
                                        </tr>
                                        <tr>
                                            <th style="text-align: left !important; width:30%;" class="fw_700">Address:</th>
                                            <td style="text-align: left !important;">
                                                {{ $allteacher->address }}
                                            </td>
                                        </tr>
                                    {{--  @endforeach  --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</div>

    </div>
</section>
@endsection
