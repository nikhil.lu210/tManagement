@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Students & Teachers</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allteachers') }}">All Teachers</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Teacher</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-student">
                    <form class="fire_input2" method="post" action="{{ Route('allteacher.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Teacher</h4>
                            </div>
                            <div class="add-car float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Teacher Name*</label>
                                            <input type="text" placeholder="ex: Mr. Alak Kanti Sharma" name="teacherName" class="input1 {{ $errors->has('teacherName') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('teacherName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('teacherName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Teacher Short Name*</label>
                                            <input type="text" placeholder="Ex: AKS" name="teacherShortName" class="input1 {{ $errors->has('teacherShortName') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('teacherShortName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('teacherShortName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Department*</label>
                                            <select name="teacherDepartment" id="" class="{{ $errors->has('teacherDepartment') ? ' is-invalid' : '' }}" required>
                                                <option value="" selected disabled>Select Department</option>
                                                @foreach($allDepartments as $department)
                                                    <option value="{{ $department->deptShortName }}">{{ $department->deptShortName }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('teacherDepartment'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('teacherDepartment') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Email*</label>
                                            <input type="email" placeholder="ex: teacher@mail.com" name="teacherEmail" class="input1 {{ $errors->has('teacherEmail') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('teacherEmail'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('teacherEmail') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Password*</label>
                                            <input type="text" id="generatepassword" placeholder="ex: x12f5w6v34w8v" name="teacherPassword" class="input1 {{ $errors->has('teacherPassword') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('teacherPassword'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('teacherPassword') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                            <label>Generate Random Password </label>
                                        <a href="#" id="generatePass"  class="btn btn-info btn-custom custom-btn btn-block btn-lg">Generate Password</a>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Add Teacher</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>


@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            $( '#generatePass' ).on( "click", generatemethod );

            function generatemethod(){
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$#@=";

                for (var i = 0; i <= 7; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                var sendpass = document.getElementById("generatepassword");
                sendpass.value = text;
            }
        });
    </script>
    
@endsection
