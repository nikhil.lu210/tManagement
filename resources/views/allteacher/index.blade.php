@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Students & Teachers</li>
                        <li class="breadcrumb-item active" aria-current="page">All Teachers</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="regular-classes">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>All Teachers</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('allteacher.create') }}" class="btn btn-info custom-btn"> Add Teacher</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-search m-b-20">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name (Full)</th>
                                            <th scope="col">Name (Short)</th>
                                            <th scope="col">Department</th>
                                            <th scope="col">Mobile Number</th>
                                            <th scope="col">Route No</th>
                                            <th scope="col">Pick Point</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($allteachers as $allteacher)
                                        @if (isset($allteacher->routeNo) && isset($allteacher->pickPoint))
    
                                        <tr>
                                            <td>{{ $allteacher->name }}</td>
                                            <td>{{ $allteacher->teacherShortName }}</td>
                                            <td>{{ $allteacher->teacherDepartment }}</td>
                                            <td>{{ $allteacher->teacherNumber }}</td>
                                            <td>{{ $allteacher->routeNo }}</td>
                                            <td>{{ $allteacher->pickPoint }}</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ route('allteacher.destroy', ['id'=>$allteacher->id])}}" class="btn btn-info custom-btn btn-sm" onclick="return confirm('Are you sure to delete this item?')">
                                                        <i class="fas fa-trash"></i> 
                                                        Delete
                                                    </a>
        
                                                    <a href="{{ route('allteacher.show', ['id'=>$allteacher->id])}}" class="btn btn-info custom-btn btn-sm">
                                                        <i class="fas fa-info-circle"></i> 
                                                        Details
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
