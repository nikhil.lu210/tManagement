@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Admin Portal</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allroutes') }}">All Routes</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Route No: {{ $allroute->routeNo }}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-route">
                    <form class="fire_input2" method="post" action="{{route('allroute.update',['id'=>$allroute->id])}}">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <div class="heading float-left">
                                    <h4>Route No: {{ $allroute->routeNo }}</h4>
                                </div>
                                <div class="back-page float-right">
                                    <a href="{{ Route('allroutes') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-custom-background">
                                    <div class="row" id="more-field">
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Route No * </label>
                                                <input type="text" value="{{ $allroute->routeNo }}" name="routeNo" class="input1 removeDis {{ $errors->has('routeNo') ? ' is-invalid' : '' }}" disabled required>
                                                @if ($errors->has('routeNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routeNo') }}</strong>
                                                </span> 
                                                @endif
                                            </div>
                                        </div>
                                        @foreach (explode(',', $allroute->stoppagePoint) as $stoppagePoint)
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Stoppage Point</label>
                                                <input type="text" value="{{ trim($stoppagePoint) }}" name="stoppagePoint[]" class="input1 removeDis {{ $errors->has('stoppagePoint') ? ' is-invalid' : '' }}" disabled required>
                                                @if ($errors->has('stoppagePoint'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('totalStoppage') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="col-md-4" id="addMore">
                                            <label style="margin-bottom: 32px;"></label>
                                            <button type="button" class="btn btn-info custom-btn btn-block btn-lg removeDis" disabled>Add stoppagePoint</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-info custom-btn" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>
    
                                            <button type="button" class="btn btn-info custom-btn hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>
    
                                            <button type="submit" class="btn btn-info custom-btn hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

<script src="{{ asset('admin-css/js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
<script>
        $(document).ready(function(){
            console.log(432);
            $("#addMore").click(function(event){
    
                console.log(123);
                event.preventDefault();
                $("#more-field").append('<div class="col-md-4">\
                        <div class="group">\
                            <label>Stoppage Point</label>\
                            <input type="text" placeholder="ex: Bondor Surma Tower" name="stoppagePoint[]" class="input1 removeDis">\
                        </div>\
                    </div>'
                );
            });
        });
    </script>
{{--  ref: https://www.youtube.com/watch?v=5cywoTxRqaI  --}}

@endsection
