@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Admin Portal</li>
                        <li class="breadcrumb-item active" aria-current="page">All Buses</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="all-bus">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>All Buses</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('allbus.create') }}" class="btn btn-info custom-btn"> Add Bus</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-search m-b-20">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Bus No</th>
                                            <th scope="col">Route No</th>
                                            <th scope="col">Driver Name</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($allbuses as $allbus)
                                        <tr>
                                            <td class="fw_700">{{ $allbus->busNo }}</td>
                                            <td>{{ $allbus->routeNo }}</td>
                                            <td>{{ $allbus->driverName }}</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ route('allbus.destroy', ['id'=>$allbus->id])}}" class="btn btn-info custom-btn btn-sm" onclick="return confirm('Are you sure to delete this item?')">
                                                        <i class="fas fa-trash"></i> 
                                                        Delete
                                                    </a>
        
                                                    <a href="{{ route('allbus.show', ['id'=>$allbus->id])}}" class="btn btn-info custom-btn btn-sm">
                                                        <i class="fas fa-info-circle"></i> 
                                                        Details
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
