@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Admin Portal</li>
                        <li class="breadcrumb-item active" aria-current="page">All Buses</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-bus">
                    <form class="fire_input2" method="post" action="{{ Route('allbus.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Buses</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('allbuses') }}" class="btn btn-info custom-btn"><i class="far fa-eye"></i></i> View Buses</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Bus No. </label>
                                            <input type="text" placeholder="ex: Dhaka Metro-A 01-0203" name="busNo" class="input1 {{ $errors->has('busNo') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('busNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('busNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Route No. </label>
                                            <select name="routeNo" id="" class="{{ $errors->has('routeNo') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Route No.</option>
                                                @foreach($allRoutes as $route)
                                                    <option value="{{ $route->routeNo }}">{{ $route->routeNo }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('routeNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routeNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Driver Name </label>
                                            <select name="driverName" id="" class="{{ $errors->has('driverName') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Driver</option>
                                                @foreach($allDrivers as $driver)
                                                    <option value="{{ $driver->driverName }}">{{ $driver->driverName }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('driverName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn custom-btn"><i class="far fa-thumbs-up"></i> Add Bus</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
