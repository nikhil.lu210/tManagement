@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Admin Portal</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allbuses') }}">All Buses</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $allbus->busNo }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-bus">
                    <form class="fire_input2" method="post" action="{{route('allbus.update',['id'=>$allbus->id])}}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>{{ $allbus->busNo }}</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('allbuses') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Bus No. </label>
                                            <input type="text" value="{{ $allbus->busNo }}" name="busNo" class="input1 removeDis {{ $errors->has('busNo') ? ' is-invalid' : '' }}" disabled required>
                                            @if ($errors->has('busNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('busNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Route No. </label>
                                            <select name="routeNo" id="routeNo" class="removeDis {{ $errors->has('routeNo') ? ' is-invalid' : '' }}" disabled required>
                                                <option value="{{$allbus->routeNo}}">{{$allbus->routeNo}}</option>
                                                @foreach($allRoutes as $route)
                                                    <option value="{{ $route->id }}">{{ $route->routeNo }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('routeNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routeNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Select Driver </label>
                                            <select name="driverName" id="driverName" class="removeDis {{ $errors->has('driverName') ? ' is-invalid' : '' }}" disabled required>
                                                <option value="{{$allbus->driverName}}">{{$allbus->driverName}}</option>
                                                @foreach($allDrivers as $driver)
                                                    <option value="{{ $driver->id }}">{{ $driver->driverName }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('driverName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('driverName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-info custom-btn" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                        <button type="button" class="btn btn-info custom-btn hiddenButton d-none" id="addDisabled"><i class="fas fa-redo-alt"></i> Cancel</button>

                                        <button type="submit" class="btn btn-info custom-btn hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
