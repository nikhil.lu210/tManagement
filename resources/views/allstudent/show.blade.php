@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Students & Teachers</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allstudents') }}">All Students</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $allstudent->name }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
                    <div class="card-header">
                        <h4 class="float-left">The Profile Of {{ $allstudent->name }}</h4>
                        <div class="back-page float-right">
                            <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="user-image">
                                    <img src="{{ asset('images/'.$allstudent->avatar) }}" alt="Profile Picture Not Found" class="img-responsive img-thumbnail rounded float-left" style="max-width:100%;" />
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="user-details">
                                    <table class="table table-bordered table-dark">
                                        <tbody>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Name:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->name }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">ID:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->studentID }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Department:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->studentDept }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Batch:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->studentBatch }} <sup>( {{ $allstudent->studentSection }} )</sup></td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Route No:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->routeNo }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Pick Point:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->pickPoint }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Mobile No:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->studentMobile }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Parents No:</th>
                                                <td style="text-align: left !important;">{{ $allstudent->parentMobile }}</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Email:</th>
                                                <td style="text-align: left !important;"><a href="#">{{ $allstudent->email }}</a></td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:30%;" class="fw_700">Address:</th>
                                                <td style="text-align: left !important;">
                                                    {{ $allstudent->studentAddress }}
                                                </td>
                                            </tr>
                                        {{--  @endforeach  --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    </div>
</section>
@endsection
