@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Students & Teachers</li>
                        <li class="breadcrumb-item"><a href="{{ Route('allstudents') }}">All Students</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Student</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="add-student">
                    <form class="fire_input2" method="post" action="{{ Route('allstudent.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Add Student</h4>
                            </div>
                            <div class="add-car float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Student ID. </label>
                                            <input type="text" placeholder="ex: 1512020210" name="studentID" class="input1 {{ $errors->has('studentID') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('studentID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentID') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Full Name </label>
                                            <input type="text" placeholder="ex: Nikhil Kurmi" name="studentName" class="input1 {{ $errors->has('studentName') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('studentName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Department </label>
                                            <select name="studentDepartment" id="" class="{{ $errors->has('studentDepartment') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Department</option>
                                                @foreach($allDepartments as $department)
                                                    <option value="{{ $department->deptShortName }}">{{ $department->deptShortName }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('studentDepartment'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentDepartment') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Batch </label>
                                            <select name="studentBatch" id="" class="{{ $errors->has('studentBatch') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Batch</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                            </select>
                                            @if ($errors->has('studentBatch'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentBatch') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fire_select2 fire_select">
                                            <label>Section </label>
                                            <select name="studentSection" id="" class="{{ $errors->has('studentSection') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Section</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                                <option value="E">E</option>
                                                <option value="F">F</option>
                                            </select>
                                            @if ($errors->has('studentSection'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('studentSection') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fire_select2 fire_select">
                                            <label>Route No</label>
                                            <select name="routeNo" id="" class="{{ $errors->has('routeNo') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Route No.</option>
                                                @foreach($allRoutes as $route)
                                                    <option value="{{ $route->routeNo }}">{{ $route->routeNo }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('routeNo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routeNo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fire_select2 fire_select">
                                            <label>Pick Point</label>
                                            <select name="pickPoint" id="" class="{{ $errors->has('pickPoint') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Pick Point</option>
                                                @foreach($allRoutes as $allRoute)
                                                    @foreach (explode(',', $allRoute->stoppagePoint) as $stoppagePoint)
                                                        <option value="{{ trim($stoppagePoint) }}">{{ trim($stoppagePoint) }}</option>
                                                    @endforeach
                                                @endforeach
                                            </select>
                                            @if ($errors->has('pickPoint'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('pickPoint') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Add Student</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
