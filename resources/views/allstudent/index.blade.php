@extends('layouts.app-admin')

@section('content')
<section class="admin-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Students & Teachers</li>
                        <li class="breadcrumb-item active" aria-current="page">All Students</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="regular-classes">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>All Students</h4>
                            </div>
                            <div class="add-car float-right">
                                <a href="{{ Route('allstudent.create') }}" class="btn btn-info custom-btn"> Add Student</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-search m-b-20">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-dark">
                                    <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Department</th>
                                        <th scope="col">Batch</th>
                                        <th scope="col">Route No</th>
                                        <th scope="col">Pick Point</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @foreach ($allstudents as $allstudent)
                                    @if($allstudent->routeNo)
                                        <tr>
                                            <td class="fw_700">{{ $allstudent->studentID }}</td>
                                            <td>{{ $allstudent->name }}</td>
                                            <td>{{ $allstudent->studentDept }}</td>
                                            <td>
                                                {{ $allstudent->studentBatch }} <sup>( {{ $allstudent->studentSection }} )</sup>
                                            </td>
                                            <td>{{ $allstudent->routeNo }}</td>
                                            <td>{{ $allstudent->pickPoint }}</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ route('allstudent.destroy', ['id'=>$allstudent->id])}}" class="btn btn-info custom-btn btn-sm" onclick="return confirm('Are you sure to delete this item?')">
                                                        <i class="fas fa-trash"></i> 
                                                        Delete
                                                    </a>
        
                                                    <a href="{{ route('allstudent.show', ['id'=>$allstudent->id])}}" class="btn btn-info custom-btn btn-sm">
                                                        <i class="fas fa-info-circle"></i> 
                                                        Details
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="float-right">
                                        {{ $allstudents->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
