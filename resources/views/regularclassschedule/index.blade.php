@extends('layouts.app')

@section('content')
<section class="student-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Student Portal</li>
                        <li class="breadcrumb-item active" aria-current="page">Regular Class Schedule</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="regular-classes">
                    <div class="card">
                        <div class="card-header">
                            <h4>Regular Class Schedules</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-search">
                                <div class="form-group m-b-20">
                                    <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Day</th>
                                            <th scope="col">Class Time</th>
                                            <th scope="col">Bus Time</th>
                                            <th scope="col">Route No</th>
                                            <th scope="col">Pick Point</th>
                                            <th scope="col">Drop Point</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="fw_700">Sunday</td>
                                            <td>10.30 am</td>
                                            <td>09.45 am</td>
                                            <td>04</td>
                                            <td>Surma Tower</td>
                                            <td>Subidbazar Point</td>
                                            <td>
                                                <button class="btn btn-info btn-sm">Details</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
