@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Settings</li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/routines') }}">Routine</a></li>
                        <li class="breadcrumb-item">
                                @if( isset($datas[0]->day) && isset($datas[0]->busTime))
                                    {{ $datas[0]->day }}
                                
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            
                                {{ $datas[0]->busTime }}
                            @endif
                        </li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="all-driver">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>
                                    @if(isset($datas[0]->day) && isset($datas[0]->busTime))
                                        {{ $datas[0]->day }} at {{ $datas[0]->busTime }}
                                    @endif
                                </h4>
                            </div>
                            <div class="back-page float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-search">
                                <div class="form-group m-b-40">
                                    <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                </div>
                            </div>
                            <div class="table-responsive m-t-20">
                                <table class="table table-bordered table-dark">
                                    <thead>
                                    <tr>
                                        <th scope="col">Batch</th>
                                        <th scope="col">Course</th>
                                        <th scope="col">Teacher</th>
                                        <th scope="col">Room</th>
                                        <th scope="col">Total Student</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($datas as $data) 
                                        <tr>
                                            <td class="fw_700">{{ $data->batch }} <sup>{{ $data->section }}</sup></td>
                                            <td class="fw_700">{{ $data->course }}</td>
                                            <td class="fw_700">{{ $data->teacher }}</td>
                                            <td class="fw_700">{{ $data->room }}</td>
                                            <td class="fw_700">45</td>
                                        </tr>
                                         @endforeach 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
