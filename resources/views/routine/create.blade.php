@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">Routine</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="upload-routine">
                    <form class="fire_input2" method="post" action="{{ Route('routine.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Upload Routine</h4>
                            </div>
                            <div class="add-car float-right">
                                <a onclick="history.back();" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="fire_select2 fire_select">
                                            <label>Select Day </label>
                                            <select name="day" id="" class="{{ $errors->has('day') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Day</option>
                                                <option value="Sunday">Sunday</option>
                                                <option value="Monday">Monday</option>
                                                <option value="Tuesday">Tuesday</option>
                                                <option value="Wednesday">Wednesday</option>
                                                <option value="Thrusday">Thrusday</option>
                                            </select>
                                            @if ($errors->has('day'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('day') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Select Routine </label>
                                            <input type="file" placeholder="ex: Routine" name="routineFile" class="input1 {{ $errors->has('routineFile') ? ' is-invalid' : '' }}" required>
                                            @if ($errors->has('routineFile'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('routineFile') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="fas fa-redo-alt"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Upload Routine</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection




