@extends('layouts.app-admin')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">Routine</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="routine">
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h4>Daily Routine</h4>
                            </div>
                            <div class="add-routine float-right">
                                <a href="{{ Route('routine.create') }}" class="btn btn-info custom-btn"> Update Routine</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="sunday-tab" data-toggle="tab" href="#sunday" role="tab" aria-controls="sunday" aria-selected="true">Sunday</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="monday-tab" data-toggle="tab" href="#monday" role="tab" aria-controls="monday" aria-selected="false">Monday</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="tuesday-tab" data-toggle="tab" href="#tuesday" role="tab" aria-controls="tuesday" aria-selected="false">Tuesday</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="wednesday-tab" data-toggle="tab" href="#wednesday" role="tab" aria-controls="wednesday" aria-selected="false">Wednesday</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="thrusday-tab" data-toggle="tab" href="#thrusday" role="tab" aria-controls="thrusday" aria-selected="false">Thrusday</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="sunday" role="tabpanel" aria-labelledby="sunday-tab">
                                        <div class="table-search m-b-20">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                            </div>
                                        </div>
                                        <div class="table-responsive m-t-20">
                                            <table class="table table-bordered table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Bus Time</th>
                                                        <th scope="col">Class Time</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="fw_700">08:30 AM</td>
                                                        <td>09.30 AM - 10.55 AM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Sunday', 'time' => '08:30 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">09:45 AM</td>
                                                        <td>11.00 AM - 12.25 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Sunday', 'time' => '09:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">11:45 AM</td>
                                                        <td>12.30 PM - 01.55 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Sunday', 'time' => '11:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">12:45 PM</td>
                                                        <td>02.00 PM - 03.30 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Sunday', 'time' => '12:45 PM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                        <div class="table-search m-b-20">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Bus Time</th>
                                                        <th scope="col">Class Time</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="fw_700">08:30 AM</td>
                                                        <td>09.30 AM - 10.55 AM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Monday', 'time' => '08:30 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">09:45 AM</td>
                                                        <td>11.00 AM - 12.25 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Monday', 'time' => '09:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">11:45 AM</td>
                                                        <td>12.30 PM - 01.55 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Monday', 'time' => '11:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">12:45 PM</td>
                                                        <td>02.00 PM - 03.30 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Monday', 'time' => '12:45 PM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tuesday" role="tabpanel" aria-labelledby="tuesday-tab">
                                        <div class="table-search m-b-20">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Bus Time</th>
                                                        <th scope="col">Class Time</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="fw_700">08:30 AM</td>
                                                        <td>09.30 AM - 10.55 AM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Tuesday', 'time' => '08:30 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">09:45 AM</td>
                                                        <td>11.00 AM - 12.25 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Tuesday', 'time' => '09:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">11:45 AM</td>
                                                        <td>12.30 PM - 01.55 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Tuesday', 'time' => '11:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">12:45 PM</td>
                                                        <td>02.00 PM - 03.30 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Tuesday', 'time' => '12:45 PM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="wednesday" role="tabpanel" aria-labelledby="wednesday-tab">
                                        <div class="table-search m-b-20">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Bus Time</th>
                                                        <th scope="col">Class Time</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="fw_700">08:30 AM</td>
                                                        <td>09.30 AM - 10.55 AM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Wednesday', 'time' => '08:30 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">09:45 AM</td>
                                                        <td>11.00 AM - 12.25 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Wednesday', 'time' => '09:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">11:45 AM</td>
                                                        <td>12.30 PM - 01.55 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Wednesday', 'time' => '11:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">12:45 PM</td>
                                                        <td>02.00 PM - 03.30 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Wednesday', 'time' => '12:45 PM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="thrusday" role="tabpanel" aria-labelledby="thrusday-tab">
                                        <div class="table-search m-b-20">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="searchTable" placeholder="Search what you need...">
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Bus Time</th>
                                                        <th scope="col">Class Time</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="fw_700">08:30 AM</td>
                                                        <td>09.30 AM - 10.55 AM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Thrusday', 'time' => '08:30 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">09:45 AM</td>
                                                        <td>11.00 AM - 12.25 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Thrusday', 'time' => '09:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">11:45 AM</td>
                                                        <td>12.30 PM - 01.55 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Thrusday', 'time' => '11:45 AM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                              
                                                    <tr>
                                                        <td class="fw_700">12:45 PM</td>
                                                        <td>02.00 PM - 03.30 PM</td>
                                                        <td>
                                                            <a href="{{ Route('routine.show', ['day' => 'Thrusday', 'time' => '12:45 PM']) }}" class="btn btn-info custom-btn btn-sm">
                                                                <i class="fas fa-info-circle"></i> 
                                                                Details
                                                            </a>
                                                        </td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection


@section('inline_js')

@endsection